import BaseModel from "./BaseModel";
import {observable} from 'mobx';

class IImage extends BaseModel {
  @observable path = {};
  @observable extension = null;
}

export default IImage;
