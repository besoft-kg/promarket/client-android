import {observable} from "mobx";
import BaseModel from "./BaseModel";
import Image from './Image';

class ProductImage extends BaseModel {
  @observable image = null;
  @observable color_id = null;

  constructor(props) {
    super(props, {
      image: Image,
    });
  }
}

export default ProductImage;
