import {getRoot, types as t} from 'mobx-state-tree';
import Product from './MSTProduct';
import {values} from 'mobx';

const Category = t
  .model('category', {
    id: t.identifierNumber,
    title: t.string,
    short_title: t.string,
    icon: t.maybeNull(t.frozen()),
    all_subcategories: t.maybeNull(t.array(t.integer)),
    colorable: t.boolean,
    parent_id: t.maybeNull(t.integer),
    products: t.optional(t.array(t.reference(Product)), []),
  })
  .actions(self => {

    const setProducts = (items) => {
      self.products = items;
    };

    return {
      setProducts,
    };
  })
  .views(self => ({

    get subcategories() {
      return values(self.root.categoryStore.items).filter(v => v.parent_id === self.id);
    },

    get root() {
      return getRoot(self);
    },

  }));

export default Category;
