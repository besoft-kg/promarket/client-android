import {action, comparer, computed, observable} from "mobx";
import BaseModel from "./BaseModel";
import CategoryProperty from "./CategoryProperty";

class CategoryGroup extends BaseModel {
    @observable category_id = null;
    @observable title = '';
    @observable position = null;

    @observable properties = observable.array([]);

    constructor(props) {
        super(props, {
            properties: CategoryProperty,
        });
    }
    _ignore_from_sync = [];
    _sync_url = '/category/group';

    @computed get can_be_saved() {
        return !comparer.structural(this._toJS(), this._synced_data) && (
            this.title.length > 0 && this.short_title.length > 0 && this.title !== 'Без названия' &&
            (this.icon_id || this.image_id || this.image || this.icon)
        );
    }

    @action addProperty() {
        this.properties.push(new CategoryProperty({
            category_group_id: this.id
        }));
    }
}

export default CategoryGroup;
