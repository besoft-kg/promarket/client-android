import {action, computed, observable} from "mobx";
import BaseModel from "./BaseModel";
import PropertyOption from "./PropertyOption";
import Color from "./Color";

const SOURCE_TEXT = {
  yandex_market: 'Яндекс.Маркет',
};

class Image extends BaseModel {
  @observable path = {};
  @observable image = null;
  @observable color = null;
  @observable color_id = null;

  constructor(props) {
    super(props, {
      options: PropertyOption,
      color: Color,
    });
  }

  @action setOption(s) {
    this.option = s;
  }

  @action setColor(s) {
    this.color = s;
  }

  @computed get valid_data() {
    return this.image;
  }

  @computed get source_text() {
    return SOURCE_TEXT[this.source];
  }
}

export default Image;
