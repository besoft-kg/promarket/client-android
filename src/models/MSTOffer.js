import {getRoot, types as t} from 'mobx-state-tree';
import Product from './MSTProduct';

const Offer = t
  .model('offer', {
    id: t.identifierNumber,
    sale_price: t.integer,
    price: t.integer,
    used: t.boolean,
    description: t.maybeNull(t.string),
    discount_active: t.boolean,
    discount_percentage: t.maybeNull(t.integer),
    discount_start_on: t.maybeNull(t.Date),
    discount_end_on: t.maybeNull(t.Date),
    product_id: t.maybeNull(t.integer),
    store_id: t.maybeNull(t.integer),
    product: t.maybeNull(t.reference(Product))
  })
  .actions(self => {

    return {};

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

  }));

export default Offer;
