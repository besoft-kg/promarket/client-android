import {getRoot, types as t} from 'mobx-state-tree';
import Product from './MSTProduct';

const Brand = t
  .model('brand', {
    id: t.identifierNumber,
    title: t.string,
    logo: t.frozen(),
    products: t.optional(t.array(t.reference(Product)), []),
  })
  .actions(self => {
    return {};
  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

  }));

export default Brand;
