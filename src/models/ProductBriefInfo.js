import {action, computed, observable} from "mobx";
import BaseModel from "./BaseModel";

class ProductBriefInfo extends BaseModel {
  @observable value = '';

  @computed get valid_data() {
    return this.value.trim().length > 0;
  }

  @action fix() {
    this.value = this.value.trim();
  }
}

export default ProductBriefInfo;
