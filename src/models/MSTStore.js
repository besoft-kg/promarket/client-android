import {getRoot, types as t} from 'mobx-state-tree';
import Offer from './MSTOffer';

const Store = t
  .model('store', {
    id: t.identifierNumber,
    title: t.maybeNull(t.string),
    contacts: t.optional(t.array(t.frozen()), []),
    logo_id: t.maybeNull(t.integer),
    offers: t.optional(t.array(t.reference(Offer)), []),

    logo: t.maybeNull(t.frozen()),
    branches: t.optional(t.array(t.frozen()), []),
  })
  .actions(self => {
    return {};
  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

  }));

export default Store;
