import {computed, observable} from 'mobx';
import BaseModel from "./BaseModel";
import IImage from './IImage';

class IProduct extends BaseModel {
  @observable main_image = null;
  @observable title = 'Без названия';
  @observable cheapest_offer = null;
  @observable discount_offer = null;
  @observable offers_count = 0;

  constructor(props) {
    super(props, {
      main_image: IImage,
    });
  }

  @computed get offers_count_ui() {
    if (this.offers_count === 0) {
      return 'Нет предложений';
    } else if (this.offers_count === 1) {
      return '1 предложение';
    } else if (this.offers_count > 1 && this.offers_count < 5) {
      return `${this.offers_count} предложения`;
    } else return `${this.offers_count} предложений`;
  }
}

export default IProduct;
