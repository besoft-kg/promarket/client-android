import {computed, observable} from "mobx";
import BaseModel from "./BaseModel";

const SOURCE_TEXT = {
  youtube: 'YouTube',
};

class ProductArticle extends BaseModel {
  @observable title = '';
  @observable url = '';
  @observable identity = '';
  @observable source = 'youtube';

  _ignore_from_sync = ['identity'];

  @computed get valid_data() {
    return this.url.length > 0 && this.title.length > 0;
  }

  @computed get source_text() {
    return SOURCE_TEXT[this.source];
  }
}

export default ProductArticle;
