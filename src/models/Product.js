import {observable, action, computed, comparer, toJS} from "mobx";
import BaseModel from "./BaseModel";
import Brand from "./Brand";
import ProductBriefInfo from "./ProductBriefInfo";
import ProductArticle from "./ProductArticle";
import Category from "./Category";
import ProductImage from './ProductImage';

class Product extends BaseModel {
  @observable images = [];
  @observable main_image = null;
  @observable title = 'Без названия';
  @observable description = '';
  modifications = observable.array([]);
  @observable category_id = null;
  @observable brand = null;
  @observable brand_id = null;
  @observable brief_info = [];
  @observable articles = [];
  @observable category = null;
  @observable colors = [];
  @observable parent_id = '';
  @observable properties = observable.array([]);
  @observable cheapest_offer = null;
  @observable popular_offer = null;
  @observable offers_count = 0;
  @observable offers = [];
  @observable categories = [];

  constructor(props) {
    super(props, {
      images: ProductImage,
      brief_info: ProductBriefInfo,
      articles: ProductArticle,
      category: Category,
      brand: Brand,
    });
  }

  @action toggleProperty(checked, property) {
    const found = this.properties.find(v => v.property_id === property.id);
    if (checked) {
      if (!found) {
        let merged_object = {};
        if (['integer', 'boolean', 'string'].includes(property.type)) merged_object = {value: ''};
        else if (property.type === 'select') merged_object = {option: null};
        else if (property.type === "multi_select") merged_object = {options: []};
        this.properties.push({
          ...merged_object,
          property_id: property.id,
        });
      }
    } else if (found) {
      this.properties.splice(this.properties.indexOf(found), 1);
    }
  }

  @action setPropertyValue(p_id, value) {
    const found = this.properties.find(v => v.property_id === p_id);
    const props = toJS(this.properties);
    if (found) {
      props[this.properties.indexOf(found)].value = value;
      this.properties = [...props];
    }
  }

  @action setPropertyOption(p_id, value) {
    const found = this.properties.find(v => v.property_id === p_id);
    const props = toJS(this.properties);
    if (found) {
      props[this.properties.indexOf(found)].option = value;
      this.properties = [...props];
    }
  }

  @action setPropertyOptions(p_id, value) {
    const found = this.properties.find(v => v.property_id === p_id);
    const props = toJS(this.properties);
    if (found) {
      props[this.properties.indexOf(found)].options = value;
      this.properties = [...props];
    }
  }

  @computed get categories_reverse() {
    return this.categories.reverse();
  }

  @action setCategories() {
    const categories = [this.category];
    if (this.category.parent) {
      let category = this.category.parent;
      do {
        categories.push(category);
      } while (category.parent);
    }
    this.categories = categories.reverse();
  }

  @action
  async setBrand(s) {
    this.brand = s;
    this.brand_id = s.id;
  }

  @action removeArticle(data) {
    this.articles = this.articles.filter(v => v !== data);
  }

  @action setCategory(s) {
    this.category = s;
    this.category_id = s.id;
    this.setCategories();
  }

  @action addImage(s) {
    this.images.push(s);
  }

  @action addArticle(s) {
    this.articles.push(s);
  }

  @action addBriefInfo(data) {
    this.brief_info.push(data);
  }

  @action deleteImage(data) {
    this.images = this.images.filter(v => v !== data);
  }

  @action removeBriefInfo(data) {
    if (this.brief_info.indexOf(data) > -1) {
      this.brief_info = this.brief_info.filter(v => v !== data);
    }
  }

  @action cancelChanges(name = null) {
    if (name && !(name in this)) return false;
    let c = null;
    if (name === 'brief_info') c = ProductBriefInfo;
    if (name) this[name] = this._synced_data[name].map(v => new c(v));
    else {
      this._update(this._synced_data);
      this.setCategories();
    }
  }

  @action setBriefInfo(m) {
    this.brief_info = m.map(v => new ProductBriefInfo(v));
  }

  @computed get options_can_be_saved() {
    return !comparer.structural(this._synced_data['modifications'], this._toJS()['modifications']);
  }

  @computed get can_be_saved() {
    if (!this.id) return this.title.length > 0 && this.title !== 'Без названия' && this.brand_id > 0 && this.category_id > 0;
    return !comparer.structural(this._toJS(), this._synced_data);
  }

  @action setOptions(s) {
    let deleted_items = this.options.map(v => v.title);

    this.options = s.map(v => {
      if (typeof v === 'string') {
        deleted_items = deleted_items.filter(k => v !== k);
        //return new ProductProperty({title: v});
      } else {
        deleted_items = deleted_items.filter(k => v.title !== k);
        //return new ProductProperty(v);
      }
    });

    this.images = this.images.map((v) => {
      if (v.option && deleted_items.includes(v.option.title)) v.option = null;
      return v;
    });

    this.brief_info = this.brief_info.map((v) => {
      if (v.option && deleted_items.includes(v.option.title)) v.option = null;
      return v;
    });

    this.articles = this.articles.map((v) => {
      if (v.option && deleted_items.includes(v.option.title)) v.option = null;
      return v;
    });
  }

  @action addColor(s, index) {
    if (index === -1) this.colors.push(s);
    else this.colors[index] = s;
  }

  @action setColors(s) {
    let deleted_items = this.colors.map(v => v.title);

    this.colors = s.map(v => {
      if (typeof v === 'string') {
        deleted_items = deleted_items.filter(k => v !== k);
        return {title: v};
      } else {
        deleted_items = deleted_items.filter(k => v.title !== k);
        return v;
      }
    });

    this.images = this.images.map((v) => {
      if (v.color && deleted_items.includes(v.color.title)) v.color = null;
      return v;
    });
  }
}

export default Product;
