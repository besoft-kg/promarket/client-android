import {types} from "mobx-state-tree";

export default types.custom({
  name: "float",
  fromSnapshot(value) {
    return Number.parseFloat(value);
  },
  toSnapshot(value) {
    return value.toString();
  },
  isTargetType(value) {
    return Number(value) === value && value % 1 !== 0;
  },
  getValidationMessage(value) {
    if (/^-?\d+\.\d+$/.test(value)) return "";
    return `'${value}' doesn't look like a valid float number`;
  }
});
