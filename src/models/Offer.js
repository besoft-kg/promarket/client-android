import {observable} from "mobx";
import BaseModel from "./BaseModel";

class Offer extends BaseModel {
  @observable store = null;
  @observable id = null;
  @observable type = null;
  @observable branch = null;
  @observable discount_active = null;
  @observable discount_percentage = null;
  @observable product = null;
  @observable sale_price = 0;
  @observable used = 0;
  @observable price = 0;
  @observable data = []; // additional properties
  @observable availability = ''; // доступность: по заказу | есть в наличии
}

export default Offer;
