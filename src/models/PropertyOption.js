import {computed, observable} from "mobx";
import BaseModel from "./BaseModel";

class PropertyOption extends BaseModel {
  @observable value = '';
  @observable property_id = null;

  @computed get valid_data() {
    return this.value.trim().length > 0;
  }
}

export default PropertyOption;
