import {action, computed, observable} from "mobx";
import BaseModel from "./BaseModel";
import PropertyOption from "./PropertyOption";

class CategoryPropertyOption extends BaseModel {
    @observable category_id = null;
    @observable category_group_id = null;
    @observable category_property_id = null;
    @observable position = null;
    @observable value = '';

    _ignore_from_sync = ['category_id','category_group_id','category_property_id'];

    @computed get valid_data() {
        return (this.title || '').trim().length > 0 && this.options.length > 0;
    }

    // @action setOptions(s) {
    //     this.options = s.map(v => {
    //         if (typeof v === 'string') return new PropertyOption({value: v});
    //         else return new PropertyOption(v);
    //     });
    // }
}

export default CategoryPropertyOption;
