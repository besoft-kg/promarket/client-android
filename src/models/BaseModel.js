import {action, observable, toJS} from "mobx";

class BaseModel {
  @observable id = null;

  _converters = null;

  constructor(props, converters = {}) {
    if (props) {
      this._converters = converters;
      this._update(props);
    }
  }

  @action _update(props) {
    Object.keys(props).map((v) => {
      if (v in this) {
        if (this._converters && this._converters[v]) {
          if (props[v] instanceof Array) {
            this[v] = props[v].map((g) => new this._converters[v](g));
          } else if (props[v] instanceof Object) {
            this[v] = new this._converters[v](props[v]);
          } else if (props[v] !== null) this[v] = props[v];
        } else if (props[v] !== null) this[v] = props[v];
      }
    });
  }

  @action _synced(data) {
    this._update(data);
    this._synced_data = this._toJS();
  }

  @action _setValue = (name, value) => {
    if (name in this) this[name] = value;
  };

  _toJS() {
    const res = {};
    const data = toJS(this);
    Object.keys(data).filter(v => !v.startsWith('_')).map(v => {
      if (data[v] instanceof Array) {
        res[v] = this[v].map(g => {
          if (typeof g === 'string') return g;
          if (typeof g === 'object' && '_toJS' in g) return g._toJS();
          return g;
        });
      } else if (data[v] instanceof Object) {
        res[v] = '_toJS' in this[v] ? this[v]._toJS() : data[v];
      } else res[v] = data[v];
    });
    return res;
  }
}

export default BaseModel;
