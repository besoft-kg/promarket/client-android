import {getRoot, types as t} from 'mobx-state-tree';
import React from 'react';

export default t
  .model('product', {
    id: t.identifierNumber,
    title: t.string,
    description: t.maybeNull(t.string),
    parent_id: t.maybeNull(t.integer),
    offers_count: t.maybeNull(t.integer),
    main_image: t.maybeNull(t.frozen()),
    cheapest_offer: t.maybeNull(t.frozen()),
    discount_offer: t.maybeNull(t.frozen()),
    images: t.optional(t.array(t.frozen()), []),
    modifications: t.optional(t.array(t.frozen()), []),
    colors: t.optional(t.array(t.frozen()), []),
    offers: t.optional(t.array(t.frozen()), []),
    brief_info: t.optional(t.array(t.frozen()), []),
    articles: t.optional(t.array(t.frozen()), []),
    brand: t.maybeNull((t.frozen())),

  }).views(self => ({

    get root() {
      return getRoot(self);
    },

    get offers_count_ui() {
      if (self.offers_count === 0) {
        return 'Нет предложений';
      } else if (self.offers_count === 1) {
        return '1 предложение';
      } else if (self.offers_count > 1 && this.offers_count < 5) {
        return `${self.offers_count} предложения`;
      } else return `${self.offers_count} предложений`;
    },

  })).actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    return {
      setValue,
    };

  });
