import {action, computed, observable} from "mobx";
import BaseModel from "./BaseModel";
import PropertyOption from "./PropertyOption";
import CategoryPropertyOption from "./CategoryPropertyOption";

class CategoryProperty extends BaseModel {
  @observable category_id = null;
  @observable category_group_id = null;
  @observable position = null;
  @observable title = '';
  @observable type = '';
  @observable suffix = null;
  @observable modifiable = false;

  @observable options = [];

  _ignore_from_sync = ['category_id', 'category_group_id'];

  @computed get valid_data() {
    return (this.title || '').trim().length > 0 && this.options.length > 0;
  }

  constructor(props) {
    super(props, {
       //option: ProductProperty,
      options: CategoryPropertyOption,
    });
  }

  @action setOptions(s) {
    this.options = s.map(v => {
      if (typeof v === 'string') return new PropertyOption({value: v});
      else return new PropertyOption(v);
    });
  }

  @action setOption(s) {
    this.option = s;
  }
}

export default CategoryProperty;
