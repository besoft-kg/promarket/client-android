import {action, comparer, computed, observable} from "mobx";
import BaseModel from "./BaseModel";
import CategoryGroup from "./CategoryGroup";

class Category extends BaseModel {
  @observable title = '';
  @observable short_title = '';
  //@observable description = '';
  @observable icon_id = null;
  @observable parent_id = null;
  @observable all_subcategories = observable.array([]);
  @observable colorable = false;
  @observable creator_id = null;
  @observable colors = [];
  @observable children = []

  @observable subcategories = observable.array([]);
  @observable icon = null;
  @observable groups = [];

  constructor(props) {
    super(props, {
      groups: CategoryGroup
    });
  }
  _ignore_from_sync = ['subcategories', 'image_id', 'icon_id', 'creator_id', 'all_subcategories'];
  _sync_url = '/category';

  @computed get can_be_saved() {
    return !comparer.structural(this._toJS(), this._synced_data) && (
      this.title.length > 0 && this.short_title.length > 0 && this.title !== 'Без названия'
    );
  }

  @action addGroup(d) {
    this.groups.push(new CategoryGroup(d));
  }

  @computed get image_url() {
    return this.icon ? 'http://api.office.besoft.com/' + this.icon.path.original : 'https://images.ebaysocial.ru/A-pHpX-hk_Lq6lLjAowmyzE6UunBGog37jfEILwRISA/s:258:258/plain/gs://coupons-data-production/gadbvsi84mmdhly8cay6nvoisa2h@png';
  };
}

export default Category;
