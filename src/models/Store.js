import {observable} from "mobx";
import BaseModel from "./BaseModel";

class Store extends BaseModel {
  @observable title = '';
  @observable contacts = [];
  @observable logo_id = null;
  @observable creator_id = null;
  @observable offers = [];

  @observable logo = null;
  @observable branches = observable.array([]);

  _ignore_from_sync = ['logo_id', 'branches','logo'];
  _sync_url = '/store';
}

export default Store;
