import {observable} from 'mobx';
import BaseModel from './BaseModel';
import Product from './Product';

class Brand extends BaseModel {
  @observable title = '';
  @observable logo = null;
  @observable logo_id = null;

  @observable products = [];

  constructor(props) {
    super(props, {
      products: Product,
    });
  }
}


export default Brand;
