import {computed, observable} from "mobx";
import BaseModel from "./BaseModel";

class Color extends BaseModel {
  @observable title = '';
  @observable hex_code = '';
  @observable category_id = null;

  _ignore_from_sync = ['category_id'];

  @computed get valid_data() {
    return this.title.trim().length > 0;
  }
}

export default Color;
