import AsyncStorage from '@react-native-async-storage/async-storage';
import {STORAGE_PREFIX} from './settings';

const get = async (name, default_value = null) => {
  try {
    const value = JSON.parse(await AsyncStorage.getItem(`@${STORAGE_PREFIX}:${name}`));
    return value || default_value;
  } catch (error) {
    console.log(error);
    return default_value;
  }
};

const set = async (name, value) => {
  try {
    value = JSON.stringify(value);
    await AsyncStorage.setItem(`@${STORAGE_PREFIX}:${name}`, value);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

const remove = async (name) => {
  try {
    await AsyncStorage.removeItem(`@${STORAGE_PREFIX}:${name}`);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export default {
  get, set, remove,
};
