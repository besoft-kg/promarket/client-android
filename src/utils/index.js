import CustomError from './exceptions';
import {Dimensions} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export const windowSize = Dimensions.get('window');
export const WINDOW_SIZE = Dimensions.get('window');

export const buildFormData = (formData, data, parentKey) => {
  if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
    Object.keys(data).forEach(key => {
      buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
    });
  } else {
    let value = data == null ? '' : data;
    if (value === true) {
      value = '1';
    } else if (value === false) {
      value = '0';
    }
    formData.append(parentKey, value);
  }
};

export const getCustomError = (e) => {
  if (!(e instanceof CustomError)) {
    e = new CustomError().setError(e);
  }
  return e;
};

export const sizeNormalizer = (size) => RFValue(size, WINDOW_SIZE.height);

export const numberWithSpaces = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
};
