import axios from 'axios';
import {buildFormData} from "./index";

export const APP_NAME = 'ProMarket';
export const APP_PRIMARY_COLOR = '#009846';
export const APP_SECONDARY_COLOR = '#ef7f1a';

export const DEBUG_MODE = false && process.env.NODE_ENV !== "production";
// export const NO_AVATAR_URL = '/images/no-avatar-300x300.png';
// export const WEB_URL = DEBUG_MODE ? "http://localhost:3000" : "https://office.promarket.kg";
export const API_VERSION = 1;
export const APP_VERSION = '0.1.6';
export const APP_VERSION_CODE = 16;
export const STORAGE_PREFIX = 'ProMarket';
export const INTRO_PAGE_VERSION = 2;
export const GPS_REQUEST_VERSION = 1;
export const API_URL = DEBUG_MODE ? 'http://192.168.0.101:8000' : 'https://promarket.kg';
export const OFFICE_API_URL = 'https://api.office.promarket.besoft.kg';
export const API_URL_WITH_VERSION = API_URL + '/api/v' + API_VERSION;

const axios_instance = axios.create({
  baseURL: API_URL_WITH_VERSION,
  responseType: 'json',
  responseEncoding: 'utf8',
});

axios_instance._configs = {
  silence: false,
};

export const requester = {
  get: (url, params = {}, silence = false) => {
    axios_instance._configs.silence = silence;
    return axios_instance.get(`${API_URL_WITH_VERSION}${url}`, {
      params,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },
  post: (url, data, silence = false) => {
    axios_instance._configs.silence = silence;
    const form_data = new FormData();
    if (data) buildFormData(form_data, data);
    return axios_instance.post(`${API_URL_WITH_VERSION}${url}`, form_data, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    });
  },
  put: (url, data, silence = false) => {
    axios_instance._configs.silence = silence;
    return axios_instance.put(`${API_URL_WITH_VERSION}${url}`, data);
  },
  delete: (url, params, silence = false) => {
    axios_instance._configs.silence = silence;
    return axios_instance.delete(`${API_URL_WITH_VERSION}${url}`, {params});
  },
};

export function initialize(appStore){

  axios_instance.interceptors.request.use((config) => {
    if (appStore.token) config.headers.common.Authorization = 'Bearer ' + appStore.token;
    return config;
  }, (error) => {
    if (!axios_instance._configs.silence) appStore.showError(error.message || error);
    if (error instanceof CustomError) throw error;
    throw new CustomError().setError(error);
  });

  axios_instance.interceptors.response.use((response) => {
    if (response.data.status < 0) {
      switch (response.data.result) {
        case 'token_is_expired':
          appStore.showInfo('Срок действия вашего токена истек, пожалуйста, войдите снова!');
          appStore.signOut();
          break;
        case 'token_is_invalid':
          appStore.showInfo('Ваш токен недействителен, пожалуйста, войдите снова!');
          appStore.signOut();
          break;
      }

      if (!axios_instance._configs.silence) {
        let message = response.data.payload;
        switch (response.data.result) {
          case 'invalid_params':
            message = 'Неверные параметры!';
            break;

          default:
            break;
        }
        appStore.showError(message);
      }

      throw new CustomError(response);
    }
    return response;
  }, (error) => {
    if (!(error instanceof CustomError)) error = new CustomError().setError(error);
    if (!axios_instance._configs.silence) appStore.showError(error.description || error.message);
    return Promise.reject(error);
  });
}
