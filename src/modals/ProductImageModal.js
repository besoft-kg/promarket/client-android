import React from 'react';
import {Image, StatusBar, View} from 'react-native';
import {Button} from 'react-native-paper';
import BaseScreen from '../screens/BaseScreen';
import {observable} from 'mobx';
import ImageZoom from 'react-native-image-pan-zoom';
import {windowSize} from '../utils';
import {OFFICE_API_URL} from '../utils/settings';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {observer} from 'mobx-react';

@observer
class ProductImageModal extends BaseScreen {
  @observable active_image = 0;

  constructor(props) {
    super(props);
    this.item = this.props.route.params.item;
    this.active_image = this.props.route.params.index;
  }

  render() {
    return (
      <>
        <StatusBar barStyle="dark-content"/>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white'}}>
          <Carousel
            containerCustomStyle={{marginBottom: 10}}
            layout={'default'}
            data={this.item}
            sliderWidth={windowSize.width}
            itemWidth={windowSize.width}
            firstItem={this.active_image}
            onBeforeSnapToItem={index => this.setValue('active_image', index)}
            renderItem={({item}) => (
              <View style={{alignItems: 'center'}}>
                <ImageZoom cropWidth={windowSize.width}
                           cropHeight={windowSize.height + 100}
                           imageWidth={250}
                           imageHeight={600}>
                  <Image style={{borderRadius: 8, width: '100%', height: '80%'}} resizeMode={'contain'}
                         source={{uri: `${OFFICE_API_URL}/${item.image.path.original}`}}/>
                </ImageZoom>
              </View>
            )}
          />
          <Pagination
            dotsLength={this.item.length}
            activeDotIndex={this.active_image}
            containerStyle={{marginTop: -100}}
            dotStyle={{
              width: 6,
              height: 6,
              marginHorizontal: -5,
              backgroundColor: 'rgba(81,187,17,0.99)',
            }}
          />
          <Button onPress={() => this.props.navigation.goBack()} title="Dismiss"/>
        </View>
      </>
    );
  }
}

export default ProductImageModal;
