import {Image, View} from 'react-native';
import {Card, Text, Title} from 'react-native-paper';
import React from 'react';
import BaseComponent from './BaseComponent';
import {useNavigation} from '@react-navigation/native';
import OfferScreen from '../screens/OfferScreen';
import {OFFICE_API_URL} from '../utils/settings';
import {numberWithSpaces, sizeNormalizer} from '../utils';

class ProductOfferItemComponent extends BaseComponent {
  render() {
    const {item, title} = this.props;
    return (
      <Card onPress={() => this.props.navigation.navigate('OfferScreen', {item})}
            style={{marginBottom: 8, marginHorizontal: 8, padding: 8}}>
        {title ? (
          <View justifyContent={'space-between'} flexDirection={'row'}>
            <Text style={{color: '#808080'}}>{title}</Text>
            <Text>{item.store.title}</Text>
          </View>
        ) : null}
        <View>
          <View>
            {item.discount_active ? (
              <Title style={{color: 'red'}}>{numberWithSpaces(item.sale_price)}{' '}<Title
                style={{textDecorationLine: 'underline', color: 'red'}}>с</Title>
                <Text>{'  '}
                  <Text style={{
                    fontSize: 13,
                    textDecorationLine: 'line-through',
                    textDecorationStyle: 'solid',
                    color: '#808080',
                  }}>{numberWithSpaces(item.price)}{' '}
                    <Text style={{textDecorationLine: 'underline', color: '#808080'}}>с</Text>
                  </Text>
                </Text>
              </Title>
            ) : (
              <Title>{numberWithSpaces(item.sale_price)}{' '}<Title style={{textDecorationLine: 'underline'}}>с</Title></Title>
            )}
          </View>
        </View>
        <View style={{
          alignItems: 'center',
          justifyContent: 'space-between',
          flexDirection: 'row',
        }}>
          <Text style={{lineHeight: 17, flexGrow: 1, flex: 1, marginRight: 5}}
                numberOfLines={2}>{item.product.title}</Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            {item.store.logo && (
              <Image style={{width: 40, marginBottom: sizeNormalizer(5), height: 40}}
                     resizeMode={'contain'}
                     source={{uri: `${OFFICE_API_URL}/${item.store.logo.path.original}`}}/>
            )}
          </View>
        </View>
        <View style={{
          alignItems: 'flex-end',
          justifyContent: 'space-between',
          flexDirection: 'row',
        }}>
          {!title && (
            <Text>{item.store.title}</Text>
          )}
          {item.used > 0 && (
            <Text style={{
              color: 'white',
              alignSelf: 'center',
              paddingHorizontal: 8,
              backgroundColor: 'blue',
              borderRadius: 5,
            }}>Б/У</Text>
          )}
        </View>
      </Card>
    );
  }
}

export default function (props) {
  const navigation = useNavigation();

  return <ProductOfferItemComponent {...props} navigation={navigation}/>;
};
