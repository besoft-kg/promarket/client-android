import React from  'react';
import {action} from 'mobx';

class BaseComponent extends React.Component{
  constructor(props) {
    super(props);

    if ('store' in this.props) {
      this.store = this.props.store;
      this.appStore = this.store.appStore;
      this.productStore = this.store.productStore;
      this.categoryStore = this.store.categoryStore;
      this.offerStore = this.store.offerStore;
      this.brandStore = this.store.brandStore;
      this.storeStore = this.store.storeStore;
    }
  }

  @action setValue = (name, value) => this[name] = value;
}

export default BaseComponent;
