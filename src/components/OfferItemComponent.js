import React from 'react';
import BaseComponent from './BaseComponent';
import {View, Image, Share} from 'react-native';
import {Caption, Card, Title, Text, Colors} from 'react-native-paper';
import {inject, observer} from 'mobx-react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useNavigation} from '@react-navigation/native';
import {APP_PRIMARY_COLOR, APP_SECONDARY_COLOR, OFFICE_API_URL} from '../utils/settings';
import {numberWithSpaces, sizeNormalizer} from '../utils';

@inject('store') @observer
class OfferItemComponent extends BaseComponent {
  static defaultProps = {
    inline: true,
    discount: false,
  };

  share = async () => {
    try {
      const result = await Share.share({
        message: `https://promarket.kg/offer/${this.props.item.id}`,
        title: `${this.props.item.product.title} в ${this.props.item.store.title}`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
        } else {
        }
      } else if (result.action === Share.dismissedAction) {
      }
    } catch (error) {
      alert(error.message);
    }
  };

  render() {
    const {item} = this.props;
    const favorite = this.appStore.favorites.find(v => v.id === item.id && v.type === 'offer');

    return (
      <View style={{margin: 6, flex: 1}}>
        <Card
          elevation={1}
          style={{
            width: this.props.inline ? sizeNormalizer(160) : '100%',
            flex: 1,
            justifyContent: 'space-between',
          }}
          onPress={() => this.props.navigation.navigate('OfferProductScreen', {offer: item})}
        >
          <View>
            <View justifyContent={'center'} alignItems={'center'} style={{height: 150}}>
              {item.product.main_image ? (
                <Image style={{backgroundColor: '#fff', width: '100%', height: 150}} resizeMode={'contain'}
                       source={{uri: `${OFFICE_API_URL}/${item.product.main_image.path.original}`}}/>
              ) : (
                <Image style={{backgroundColor: '#fff', width: '40%', height: '40%'}} resizeMode={'contain'}
                       source={require('../assets/images/logo-opacity-70-320-354.png')}/>
              )}
            </View>
            {item.discount_active === 1 && (
              <View style={{top: -5, left: 0, textAlign: 'center', position: 'absolute', width: 50, height: 50}}>
                <Image style={{top: 0, left: 0, position: 'absolute', width: 50, height: 50}} resizeMode={'contain'}
                       source={{uri: `https://static.wixstatic.com/media/5b5a42_3c1d848bdc2644feb2ab614c797a988b~mv2.png/v1/fill/w_600,h_462,al_c,q_85,usm_0.66_1.00_0.01/%D0%B2%D0%B7%D1%80%D1%8B%D0%B2.webp`}}/>
                <View>
                  <Text style={{top: 14, left: 14, fontWeight: 'bold', position: 'absolute', color: '#fff'}}>
                    {item.discount_percentage}%
                  </Text>
                </View>
              </View>
            )}
            {item.used > 0 && (
              <View style={{bottom: 0, left: 0, position: 'absolute', width: 40}}>
                <Text style={{
                  bottom: 6, left: 6,
                  position: 'absolute',
                  color: 'white',
                  paddingHorizontal: 8,
                  width: 40,
                  backgroundColor: 'blue',
                  borderRadius: 5,
                }}>Б/У</Text>
              </View>
            )}
          </View>

          <View style={{padding: 6}}>
            <Text>
              <>
                <Title style={{fontSize: sizeNormalizer(18), color: APP_PRIMARY_COLOR}}>
                  {numberWithSpaces(item.sale_price)}{' '}
                  <Title style={{
                    textDecorationLine: 'underline',
                    fontSize: sizeNormalizer(17),
                    color: APP_PRIMARY_COLOR,
                  }}>с</Title>
                  {item.discount_active === 1 && (
                    <Text>
                      {' '}
                      <Text style={{
                        fontSize: sizeNormalizer(13),
                        textDecorationLine: 'line-through',
                        textDecorationStyle: 'solid',
                        color: APP_SECONDARY_COLOR,
                      }}>{numberWithSpaces(item.price)} <Text
                        style={{textDecorationLine: 'underline', color: APP_SECONDARY_COLOR}}>с</Text></Text>
                    </Text>
                  )}
                </Title>
              </>
            </Text>
            <Text>{item.product.title}</Text>
          </View>
          <View style={{
            flexGrow: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'flex-end',
            padding: sizeNormalizer(5),
          }}>
            <Caption style={{lineHeight: 13, marginTop: 6, flexGrow: 1, flex: 1}}
                     numberOfLines={1}>{item.store.title}</Caption>
            <View style={{flexDirection: 'row', marginTop: 6}}>
              <Icon
                name="share-variant"
                color={'#808080'}
                size={26}
                onPress={() => this.share()}
              />
              <Icon
                style={{paddingLeft: 8}} size={27} color={favorite ? Colors.red600 : '#808080'}
                //onPress={() => this.appStore.toggleFavorite(item.id, 'offer', item)}
                name={favorite ? 'heart' : 'heart-plus-outline'}
              />
            </View>
          </View>
        </Card>
      </View>
    );
  }
};

export default function (props) {
  const navigation = useNavigation();

  return <OfferItemComponent {...props} navigation={navigation}/>;
};
