import React from 'react';
import BaseComponent from './BaseComponent';
import {FlatList, TouchableOpacity, View} from 'react-native';
import {inject, observer} from 'mobx-react';
import {Dialog, Portal, Text} from 'react-native-paper';

@inject('store') @observer
class RegionModalComponent extends BaseComponent {
  render() {
    return (
      <Portal>
        <Dialog
          visible={this.appStore.region_show && this.appStore.is_ready && this.appStore.regions.length > 0}
          onDismiss={() => this.appStore.setValue('region_show', false)}
        >
          <View style={{width: '100%', padding: 4}}>
            <TouchableOpacity
              onPress={() => this.appStore.setRegion(null, true)}
              style={{
                padding: 8,
                height: 40,
                backgroundColor: '#D7DBDD',
              }}>
              <Text style={{alignSelf: 'center'}} numberOfLines={3}>Кыргызстан</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            numColumns={2}
            data={this.appStore.regions.filter(v => !v.parent_id)}
            renderItem={({item: v}) => (
              <View style={{width: '50%', padding: 4}}>
                <TouchableOpacity
                  onPress={() => this.appStore.setRegion(v, true)}
                  style={{
                    padding: 8,
                    height: 40,
                    backgroundColor: '#D7DBDD',
                  }}>
                  <Text style={{alignSelf: 'center'}} numberOfLines={3}>{v.title}</Text>
                </TouchableOpacity>
              </View>
            )}
          />
        </Dialog>
      </Portal>
    );
  }
}

export default RegionModalComponent;
