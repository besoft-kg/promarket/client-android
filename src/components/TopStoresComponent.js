import React from 'react';
import BaseComponent from './BaseComponent';
import {View, Image, Text, FlatList} from 'react-native';
import {Card, Title} from 'react-native-paper';
// import {Rating} from 'react-native-ratings';
import {observer} from 'mobx-react';
import {useNavigation} from '@react-navigation/native';
import {OFFICE_API_URL} from '../utils/settings';

@observer
class TopStoresComponent extends BaseComponent {
  render() {
    return (
      <View style={{marginBottom: 20}}>
        <Title style={{color: '#808B96', marginTop: 20}}>
          Топ {this.props.items.length} магазины
        </Title>
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal
          renderItem={({item}) => (
            <>
              <Card style={{marginRight: 6, width: 180}}
                    onPress={() => this.props.navigation.navigate('StoreScreen', {item})}>
                <View style={{padding: 5}}>
                  <Image style={{backgroundColor: '#fff', width: '100%', height: 120}} resizeMode={'contain'}
                         source={{uri: `${OFFICE_API_URL}/${item.logo.path.original}`}}/>
                </View>
                {/*<Rating*/}
                {/*  style={{paddingVertical: 5}}*/}
                {/*  imageSize={20}*/}
                {/*  readonly={true}*/}
                {/*  startingValue={5}*/}
                {/*/>*/}
                <Text style={{textAlign: 'center', marginBottom: 8}}>
                  {item.title}
                </Text>
              </Card>
            </>
          )}
          data={this.props.items}
        />
      </View>
    );
  }
}

export default function (props) {
  const navigation = useNavigation();

  return <TopStoresComponent {...props} navigation={navigation}/>;
};
