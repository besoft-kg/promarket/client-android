import React from 'react';
import BaseComponent from './BaseComponent';
import {View, FlatList} from 'react-native';
import Title from '../ui/Title';
import {observer} from 'mobx-react';
import {useNavigation} from '@react-navigation/native';
import ProductItemComponent from './ProductItemComponent';

@observer
class SectionsComponent extends BaseComponent {
  render() {
    return (
      <>
        {this.props.items.map((v, i) => (
          <View key={i}>
            <Title style={{color: '#808B96'}}>{v.title}</Title>
            <FlatList
              keyExtractor={item => item.id.toString()}
              showsHorizontalScrollIndicator={false}
              horizontal
              renderItem={({item}) => (
                <ProductItemComponent item={item}/>
              )}
              data={v.payload}
            />
          </View>
        ))}
      </>
    );
  }
}

export default function (props) {
  const navigation = useNavigation();

  return <SectionsComponent {...props} navigation={navigation}/>;
};
