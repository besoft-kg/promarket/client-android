import React from 'react';
import BaseComponent from './BaseComponent';
import {View, FlatList} from 'react-native';
import Title from '../ui/Title';
import {observer} from 'mobx-react';
import {useNavigation} from '@react-navigation/native';
import ProductItemComponent from './ProductItemComponent';

@observer
class DiscountsComponent extends BaseComponent {
  render() {
    return (
      <>
        <View>
          {this.props.items.length === 0 ? null : (
            <Title style={{color: '#808B96'}}>
              Товары со скидкой
            </Title>
          )}
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal
            keyExtractor={item => item.id.toString()}
            renderItem={({item}) => (
              <ProductItemComponent discount item={item}/>
            )}
            data={this.props.items}
          />
        </View>
      </>
    );
  }
}

export default function (props) {
  const navigation = useNavigation();

  return <DiscountsComponent {...props} navigation={navigation}/>;
};
