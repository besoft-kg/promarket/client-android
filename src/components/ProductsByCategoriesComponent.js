import React from 'react';
import BaseComponent from './BaseComponent';
import {View, FlatList} from 'react-native';
import Title from '../ui/Title';
import {inject, observer} from 'mobx-react';
import {useNavigation} from '@react-navigation/native';
import ProductItemComponent from './ProductItemComponent';
import {values} from 'mobx';

@inject('store')
@observer
class ProductsByCategoriesComponent extends BaseComponent {
  render() {
    return (
      <>
        {values(this.categoryStore.items).filter(v => !v.parent_id && v.products.length > 0).map((v, i) => (
          <View key={i}>
            <Title style={{color: '#808B96'}}>{v.title}</Title>
            <FlatList
              showsHorizontalScrollIndicator={false}
              horizontal
              keyExtractor={item => item.id.toString()}
              renderItem={({item}) => (
                <ProductItemComponent item={item}/>
              )}
              data={v.products.slice(0, 10)}
            />
          </View>
        ))}
      </>
    );
  }
}

export default function (props) {
  const navigation = useNavigation();

  return <ProductsByCategoriesComponent {...props} navigation={navigation}/>;
};
