import React from 'react';
import BaseComponent from './BaseComponent';
import { View, Image, Text, FlatList } from 'react-native';
import { Card } from 'react-native-paper';
import Title from '../ui/Title';
import { inject, observer } from 'mobx-react';
import { useNavigation } from '@react-navigation/native';
import { APP_PRIMARY_COLOR, OFFICE_API_URL } from '../utils/settings';
import BrandsScreen from '../screens/BrandsScreen';
import { sizeNormalizer } from '../utils';

@inject('store') @observer
class PopularBrandsComponent extends BaseComponent {
  render() {
    return (
      <>
        <View style={{ marginBottom: 10 }}>
          <View style={{flex: 1,flexDirection: 'row',justifyContent: 'space-between', alignItems: 'center'}}>
            <Title style={{ color: '#808B96' }}>
              Популярные бренды
            </Title>
            <Title style={{color: APP_PRIMARY_COLOR, borderWidth: 1, paddingHorizontal: sizeNormalizer(8), borderRadius: 10, borderColor: APP_PRIMARY_COLOR, marginRight: sizeNormalizer(3)}} onPress={() => this.props.navigation.navigate('BrandsScreen', {items: this.props.items})}>Все</Title>
          </View>
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal
            keyExtractor={item => item.id.toString()}
            renderItem={({ item }) => (
              <>
                <Card style={{ margin: 6, width: 100, padding: 5 }}
                      onPress={() => this.props.navigation.navigate('BrandScreen', { id: item.id })}>
                  <Image style={{ backgroundColor: '#fff', width: '100%', height: 80 }} resizeMode={'contain'}
                         source={{ uri: item.logo ? `${OFFICE_API_URL}/${item.logo.path.original}` : '' }} />
                  <Text style={{ textAlign: 'center', marginVertical: 12, marginBottom: 8 }}>
                    {item.title}
                  </Text>
                </Card>
              </>
            )}
            data={this.brandStore.popular.slice()}
          />
        </View>
      </>
    );
  }
}

export default function(props) {
  const navigation = useNavigation();

  return <PopularBrandsComponent {...props} navigation={navigation} />;
};
