import React from "react";
import BaseComponent from "./BaseComponent";
import { View, Text, Image, Linking, TouchableOpacity, Alert, PermissionsAndroid } from "react-native";
import { Card, Caption } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";
import moment from "moment";
import {OFFICE_API_URL} from '../utils/settings';
import haversine from "haversine-distance";
import { inject, observer } from "mobx-react";
import Geolocation from "react-native-geolocation-service";

const timeToMoment = (s) => {
  const l = s.split(':');
  return moment().set({hour: l[0], minute: l[1], second: 0});
};

const day_name = moment().format("ddd").toLowerCase();

@inject("store") @observer
class BranchItemComponent extends BaseComponent {
  openMap = (item) => {
    if (!item.address_lat || !item.address_lng) {
      return;
    }
    Linking.openURL(`geo:${item.address_lat},${item.address_lng}`).then(() => {
    });
  };

  notGrantedLocationPermissions = async () => {
    const coarse = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
    const fine = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

    if (!coarse) {
      return PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION;
    }
    if (!fine) {
      return PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION;
    }
    return null;
  };

  getCurrentPosition = async () => {
    const not_granted_permissions = await this.notGrantedLocationPermissions();
    if (!not_granted_permissions) {
      Geolocation.getCurrentPosition(
        (position) => {
          this.appStore.setLocation(position.coords.latitude, position.coords.longitude);
        },
        (error) => {
          console.log(error.code, error.message);
        },
        {
          enableHighAccuracy: true,
          distanceFilter: 1,
          forceRequestLocation: true,
        },
      );
    } else {
      this.requestLocationPermissions(not_granted_permissions).then();
    }
  };

  requestLocationPermissions = async (permission) => {
    try {
      const granted = await PermissionsAndroid.request(permission);
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.getCurrentPosition().then();
      } else {
        // TODO: text to disabled GPS alert
        alert('GPS permission is not granted. Текст жазуу керек. Something...');
      }
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const { branch: v, store } = this.props.item;
    return (
      <>
        <Card onPress={() => this.openMap(v)} style={{ marginBottom: 8 }}>
          <Card.Content>
            <View style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}>
              <View flex={1}>
                <Text style={{ fontSize: 18, marginBottom: 8 }}>{v.address}</Text>
                {v.schedule && day_name in v.schedule && (
                  <>
                    {moment().isBetween(timeToMoment(v.schedule[day_name].open), timeToMoment(v.schedule[day_name].close)) ? (
                      <>
                        <Text style={{ color: "#368BB0" }}>
                          {v.schedule[day_name].open} - {v.schedule[day_name].close}
                          {'  '}
                          <Text style={{ color: "#1676A0" }}>
                            открыто
                          </Text>
                        </Text>
                      </>
                    ) : (
                      <>
                        <Text style={{ color: "#BF3250" }}>
                          {v.schedule[day_name].open} - {v.schedule[day_name].close}
                          {'  '}
                          <Text style={{ color: "#C6052E" }}>
                            закрыто
                          </Text>
                        </Text>
                      </>
                    )}
                  </>
                )}
              </View>
              <View>
                {store && store.logo && (
                  <Image resizeMode={"contain"}
                         source={{ uri: `${OFFICE_API_URL}/${store.logo.path.original}` }}
                         style={{ width: 60, height: 32, alignSelf: "flex-end" }} />
                )}
                {this.appStore.location_lat && this.appStore.location_lng && v.address_lat && v.address_lat ? (
                  <Text>
                    {(haversine({ lat: this.appStore.location_lat, lng: this.appStore.location_lng }, {
                      lat: v.address_lat,
                      lon: v.address_lng,
                    }) / 1000).toFixed(2)} км от Вас
                  </Text>
                ) : (
                  <TouchableOpacity onPress={() => {
                    Alert.alert(
                      "Данные о местоположении",
                      "Дайте разрешение на использование GPS чтобы посмотреть на расстояние до магазина!",
                      [
                        {
                          text: "Разрешить",
                          onPress: () => this.getCurrentPosition(),
                          style: "cancel",
                        },
                        { text: "Отмена" },
                      ],
                      { cancelable: false },
                    );
                  }}>
                    <Caption style={{ lineHeight: 15 }}>Посмотреть{"\n"}расстояние</Caption>
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </Card.Content>
        </Card>
      </>
    );
  }
}

export default function(props) {
  const navigation = useNavigation();

  return <BranchItemComponent {...props} navigation={navigation} />;
};
