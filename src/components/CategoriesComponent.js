import React from 'react';
import BaseComponent from './BaseComponent';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {observer} from 'mobx-react';
import {observable} from 'mobx';
import {useNavigation} from '@react-navigation/native';
import {SvgUri} from 'react-native-svg';
import {OFFICE_API_URL} from '../utils/settings';
import {windowSize, sizeNormalizer} from '../utils';

@observer
class CategoriesComponent extends BaseComponent {
  @observable active_slide = 0;

  _renderItem = ({item, index}) => {
    return (
      <View key={index} flexWrap={'wrap'} flexDirection={'row'} style={{paddingLeft: 8}} justifyContent={'space-between'}>
        {item.map((v, i) => (
          <TouchableOpacity
            key={i}
            onPress={() => this.props.onSelectItem ? this.props.onSelectItem(v) : this.props.navigation.navigate('CategoryScreen', {item: v})}
            style={{
              marginBottom: sizeNormalizer(5),
              padding: sizeNormalizer(8),
              alignItems: 'center',
              flexDirection: 'row',
              height: sizeNormalizer(40),
              width: '48%',
              marginRight: sizeNormalizer(5),
              backgroundColor: '#D7DBDD',
            }}>
            {v.icon && (
              <>
                {v.icon.extension === 'svg' ? (
                  <SvgUri
                    width={sizeNormalizer(20)}
                    height={sizeNormalizer(20)}
                    fill="black"
                    uri={`${OFFICE_API_URL}/${v.icon.path.original}`}
                  />
                ) : (
                  <Image
                    style={{width: sizeNormalizer(20), height: sizeNormalizer(20)}}
                    source={{uri: `${OFFICE_API_URL}/${v.icon.path.original}`}}
                  />
                )}
              </>
            )}
            <Text style={{flex: 1, fontSize: sizeNormalizer(14), marginLeft: sizeNormalizer(6)}} numberOfLines={2}>{v.short_title}</Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  render() {
    const categories_data = [];
    const {items} = this.props;

    for (let i = 0; i < Math.ceil(items.length / 6); i++) {
      categories_data.push(items.slice(i * 6, (i * 6) + 6));
    }

    return (
      <View style={{marginTop: sizeNormalizer(8), alignItems: 'center'}}>
        <Carousel
          layout={'default'}
          data={categories_data}
          sliderWidth={windowSize.width}
          itemWidth={windowSize.width}
          renderItem={this._renderItem}
          onSnapToItem={index => this.setValue('active_slide', index)}/>
        <Pagination
          dotsLength={Math.ceil(items.length / 6)}
          activeDotIndex={this.active_slide}
          containerStyle={{marginVertical: sizeNormalizer(-20)}}
          dotStyle={{
            width: sizeNormalizer(6),
            height: sizeNormalizer(6),
            marginHorizontal: sizeNormalizer(-5),
            backgroundColor: 'rgba(81,187,17,0.99)',
          }}
        />
      </View>
    );
  }
}

export default function (props) {
  const navigation = useNavigation();

  return <CategoriesComponent {...props} navigation={navigation}/>;
};
