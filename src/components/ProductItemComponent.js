import React from 'react';
import BaseComponent from './BaseComponent';
import {View, Image, Share} from 'react-native';
import {Caption, Card, Title, Text, Colors} from 'react-native-paper';
import {inject, observer} from 'mobx-react';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {useNavigation} from '@react-navigation/native';
import ProductScreen from '../screens/ProductScreen';
import {APP_PRIMARY_COLOR, APP_SECONDARY_COLOR, OFFICE_API_URL} from '../utils/settings';
import {numberWithSpaces, sizeNormalizer} from '../utils';

@inject('store') @observer
class ProductItemComponent extends BaseComponent {
  static defaultProps = {
    inline: true,
    discount: false,
  };

  share = async () => {
    try {
      const result = await Share.share({
        message: `https://promarket.kg/product/${this.props.item.id}`,
        title: `${this.props.item.title}`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
        } else {
        }
      } else if (result.action === Share.dismissedAction) {
      }
    } catch (error) {
      alert(error.message);
    }
  };

  render() {
    const {item} = this.props;
    const favorite = this.appStore.favorites.find(v => v.id === item.id && v.type === 'product');

    return (
      <View style={{margin: sizeNormalizer(6), flex: 1}}>
        <Card elevation={1}
              style={{width: this.props.inline ? sizeNormalizer(160) : '100%', flex: 1, justifyContent: 'space-between'}}
              onPress={() => this.props.navigation.navigate('ProductScreen', {id: item.id})}>
          <View>
            <View justifyContent={'center'} alignItems={'center'}
                  style={{height: sizeNormalizer(150), marginTop: sizeNormalizer(4)}}>
              {item.main_image ? (
                <Image style={{backgroundColor: '#fff', width: '100%', height: sizeNormalizer(150)}}
                       resizeMode={'contain'}
                       source={{uri: `${OFFICE_API_URL}/${item.main_image.path.original}`}}/>
              ) : (
                <Image style={{backgroundColor: '#fff', width: '40%', height: '40%'}} resizeMode={'contain'}
                       source={require('../assets/images/logo-opacity-70-320-354.png')}/>
              )}
            </View>
            {item.cheapest_offer && item.cheapest_offer.discount_active === 1 && (
              <View style={{
                top: -5,
                left: 0,
                textAlign: 'center',
                position: 'absolute',
                width: sizeNormalizer(50),
                height: sizeNormalizer(50),
              }}>
                <Image style={{
                  top: 0,
                  left: 0,
                  position: 'absolute',
                  width: sizeNormalizer(50),
                  height: sizeNormalizer(50),
                }} resizeMode={'contain'}
                       source={require('../assets/images/discount.webp')}/>
                <View>
                  <Text style={{
                    top: sizeNormalizer(14),
                    left: sizeNormalizer(14),
                    fontWeight: 'bold',
                    position: 'absolute',
                    color: '#fff',
                  }}>
                    {item.cheapest_offer.discount_percentage}%
                  </Text>
                </View>
              </View>
            )}
          </View>

          <View style={{padding: 6}}>
            <Text>
              {item.cheapest_offer ? (
                <>
                  от <Title style={{fontSize: sizeNormalizer(18), color: APP_PRIMARY_COLOR}}>{numberWithSpaces(item.cheapest_offer.price)} <Title
                  style={{textDecorationLine: 'underline', color: APP_PRIMARY_COLOR, fontSize: sizeNormalizer(17)}}>с</Title>
                  {item.cheapest_offer.discount_active === 1 && (
                    <Text>
                      {' '}
                      <Text style={{
                        fontSize: sizeNormalizer(13),
                        textDecorationLine: 'line-through',
                        textDecorationStyle: 'solid',
                        color: APP_SECONDARY_COLOR,
                      }}>{numberWithSpaces(item.cheapest_offer.price)} <Text
                        style={{textDecorationLine: 'underline', color: APP_SECONDARY_COLOR}}>с</Text></Text>
                    </Text>
                  )}
                </Title>
                </>
              ) : (
                <>
                  <Title style={{color: '#808080', fontSize: sizeNormalizer(16)}}>Нет в продаже</Title>
                </>
              )}
            </Text>

            <Text numberOfLines={3}>{item.title}</Text>
          </View>
          <View style={{
            flex: 1,
            justifyContent: 'flex-end',
            paddingHorizontal: 6,
            paddingBottom: 6,
          }}>
            <View style={{
              alignItems: 'flex-end',
              justifyContent: 'space-between',
              flexDirection: 'row',
            }}>
              <Caption style={{lineHeight: 13, flexGrow: 1, flex: 1}} numberOfLines={1}>{item.offers_count_ui}</Caption>
              <View style={{flexDirection: 'row'}}>
                <Icon
                  name="share-variant"
                  color={'#808080'}
                  size={25}
                  onPress={() => this.share()}
                />
                <Icon style={{paddingLeft: 8}} size={27} color={favorite ? Colors.red500 : '#808080'}
                      onPress={() => this.appStore.toggleFavorite(item.id, 'product', item)}
                      name={favorite ? 'heart' : 'heart-plus-outline'}/>
              </View>
            </View>
          </View>
        </Card>
      </View>
    );
  }
};

export default function (props) {
  const navigation = useNavigation();

  return <ProductItemComponent {...props} navigation={navigation}/>;
};
