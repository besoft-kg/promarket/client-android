import React from 'react';
import BaseComponent from './BaseComponent';
import {View, FlatList} from 'react-native';
import Title from '../ui/Title';
import {observer} from 'mobx-react';
import {useNavigation} from '@react-navigation/native';
import OfferItemComponent from './OfferItemComponent';

@observer
class OnlyTodayComponent extends BaseComponent {
  render() {
    return (
      <>
        <View>
          {this.props.items.length === 0 ? null : (
            <Title style={{color: '#808B96'}}>
              Только сегодня, успейте купить!
            </Title>
          )}
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal
            keyExtractor={item => item.id.toString()}
            renderItem={({item}) => (
              <OfferItemComponent item={item}/>
            )}
            data={this.props.items}
          />
        </View>
      </>
    );
  }
}

export default function (props) {
  const navigation = useNavigation();

  return <OnlyTodayComponent {...props} navigation={navigation}/>;
};
