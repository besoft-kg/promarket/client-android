import React from 'react';
import BaseComponent from './BaseComponent';
import {View, FlatList} from 'react-native';
import Title from '../ui/Title';
import {observer} from 'mobx-react';
import {useNavigation} from '@react-navigation/native';
import ProductItemComponent from './ProductItemComponent';

@observer
class PopularProductsComponent extends BaseComponent {
  render() {
    return (
      <>
        <View>
          <Title style={{color: '#808B96'}}>Популярные товары</Title>
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal
            keyExtractor={item => item.id.toString()}
            renderItem={({item}) => (
              <ProductItemComponent item={item}/>
            )}
            data={this.props.items.slice()}
          />
        </View>
      </>
    );
  }
}

export default function (props) {
  const navigation = useNavigation();

  return <PopularProductsComponent {...props} navigation={navigation}/>;
};
