import React from 'react';
import BaseComponent from './components/BaseComponent';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Geolocation from 'react-native-geolocation-service';
import HomeStack from './stacks/HomeStack';
import DiscountsStack from './stacks/DiscountsStack';
import {Linking, PermissionsAndroid, StatusBar} from 'react-native';
import {inject, observer} from 'mobx-react';
import ProfileStack from './stacks/ProfileStack';
import {
  APP_PRIMARY_COLOR,
  APP_VERSION_CODE,
  GPS_REQUEST_VERSION,
  requester,
} from './utils/settings';
import RegionModalComponent from './components/RegionModalComponent';
import {createStackNavigator} from '@react-navigation/stack';
import ProductImageModal from './modals/ProductImageModal';
import FavoritesStack from './stacks/FavoritesStack';
import storage from './utils/storage';
import {observable} from 'mobx';
import {Button, Dialog, Portal, Text} from 'react-native-paper';
import {sizeNormalizer} from './utils';
import IntroPageScreen from './screens/IntroPageScreen';

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

@inject('store') @observer
class App extends BaseComponent {
  @observable gps_request_version = null;
  @observable last_version = null;

  constructor(props) {
    super(props);
    this.getCurrentPosition().then();
  }

  getCurrentPosition = async () => {
    const not_granted_permissions = await this.notGrantedLocationPermissions();
    if (!not_granted_permissions) {
      Geolocation.getCurrentPosition(
        (position) => {
          this.appStore.setLocation(position.coords.latitude, position.coords.longitude);
        },
        (error) => {
          console.log(error.code, error.message);
        },
        {
          enableHighAccuracy: true,
          distanceFilter: 1,
          forceRequestLocation: true,
        },
      );
    } else {
      this.requestLocationPermissions(not_granted_permissions).then();
    }
  };

  notGrantedLocationPermissions = async () => {
    const coarse = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
    const fine = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

    if (!coarse) {
      return PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION;
    }
    if (!fine) {
      return PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION;
    }

    return null;
  };

  componentDidMount() {
    this.appStore.fetchItemsFromStorage();
    requester.get('/region', {}, true).then((response) => {
      this.appStore.setRegions(response.data, true).then();
    }).catch((e) => {
      console.log(e);
    });
    requester.get('/version/last', {platform: 'android'}, true).then((response) => {
      this.setValue('last_version', response.data);
    }).catch((e) => {
      console.log(e.response);
    });
  }

  requestLocationPermissions = async (permission) => {
    this.setValue('gps_request_version', await storage.get('gps_request_version', 0));
    if (this.gps_request_version >= GPS_REQUEST_VERSION) {
      return;
    }
    try {
      const granted = await PermissionsAndroid.request(permission);
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.getCurrentPosition().then();
      } else {
        // TODO: text to disabled GPS alert
        //alert("GPS permission is not granted. Текст жазуу керек. Something...");
        await storage.set('gps_request_version', GPS_REQUEST_VERSION);
      }
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    return (
      <>
        {this.last_version && (
          <Portal>
            <Dialog visible={this.last_version.version_code > APP_VERSION_CODE}>
              <Dialog.Title>Новая версия, встрейчайте!</Dialog.Title>
              <Dialog.Content>
                <Text>Мы сделали приложение еще лучше! Вышла новая версия приложения. Пожалуйста, чтобы продолжить
                  пользоватся приложением, обновите его до последней версии ({this.last_version.version_name}).</Text>
              </Dialog.Content>
              <Dialog.Actions>
                {/*<Button>Отмена</Button>*/}
                <Button
                  icon={(props) => <MaterialCommunityIcons name={'download'} {...props} />}
                  mode={'outlined'}
                  style={{width: '100%'}}
                  labelStyle={{color: APP_PRIMARY_COLOR}}
                  onPress={() => Linking.openURL(this.last_version.link)}
                >Обновить</Button>
              </Dialog.Actions>
            </Dialog>
          </Portal>
        )}

        <RegionModalComponent/>

        <StatusBar translucent backgroundColor="transparent"/>

        <Stack.Navigator
          mode={'modal'}
          initialRouteName={'Main'}
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen
            name={'ProductImageModal'}
            component={ProductImageModal}
          />

          <Stack.Screen
            name={'IntroPageScreen'}
            component={IntroPageScreen}
          />

          <Stack.Screen name={'Main'}>
            {() => (
              <Tab.Navigator
                shifting={true}
                barStyle={{backgroundColor: APP_PRIMARY_COLOR}}
              >
                <Tab.Screen
                  name="Home"
                  component={HomeStack}
                  options={{
                    tabBarLabel: `Главная`,
                    tabBarIcon: ({color}) => (
                      <MaterialCommunityIcons name="home" color={color} size={sizeNormalizer(25)}/>
                    ),
                  }}
                />
                <Tab.Screen
                  name="Discounts"
                  component={DiscountsStack}
                  options={{
                    tabBarLabel: 'Скидки',
                    tabBarIcon: ({color}) => (
                      <MaterialCommunityIcons name="sale" color={color} size={sizeNormalizer(25)}/>
                    ),
                  }}
                />
                <Tab.Screen
                  name="Favorites"
                  component={FavoritesStack}
                  options={{
                    tabBarLabel: 'Избранные',
                    tabBarIcon: ({color}) => (
                      <MaterialCommunityIcons name="heart" color={color} size={sizeNormalizer(25)}/>
                    ),
                  }}
                />
                <Tab.Screen
                  name="Profile"
                  component={ProfileStack}
                  options={{
                    tabBarLabel: 'Меню',
                    tabBarIcon: ({color}) => (
                      <MaterialCommunityIcons name="dots-horizontal" color={color} size={sizeNormalizer(25)}/>
                    ),
                  }}
                />
              </Tab.Navigator>
            )}
          </Stack.Screen>
        </Stack.Navigator>
      </>
    );
  }
}

export default App;
