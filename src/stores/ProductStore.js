import {getRoot, types as t} from 'mobx-state-tree';
import Product from '../models/MSTProduct';
import {toJS} from 'mobx';

export default t
  .model('ProductStore', {

    items: t.optional(t.map(Product), {}),
    popular: t.optional(t.array(t.reference(Product)), []),
    discount: t.optional(t.array(t.reference(Product)), []),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      item = {...(self.items.has(item.id) ? toJS(self.items.get(item.id)) : {}), ...item};

      if (item.brand) self.root.brandStore.createOrUpdate(item.brand);

      self.items.set(item.id, {
        ...item,
      });
    };

    const clearItems = () => {
      self.items = {};
    };

    return {
      setValue,
      createOrUpdate,
      clearItems,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

  }));
