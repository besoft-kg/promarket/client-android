import {flow, getRoot, types as t} from 'mobx-state-tree';
import storage from '../utils/storage';
import float from '../models/float';
import {toJS} from 'mobx';

export default t
  .model('AppStore', {

    regions: t.optional(t.array(t.frozen()), []),
    region: t.maybeNull(t.frozen()),
    location_lat: t.maybeNull(float),
    location_lng: t.maybeNull(float),
    is_ready: t.optional(t.boolean, false),
    region_show: t.optional(t.boolean, false),
    intro_page_version: t.maybeNull(t.integer),
    favorites: t.optional(t.array(t.frozen()), []),
    // store: t.maybeNull(Store),
    // branch: t.maybeNull(t.reference(Branch)),
    // user: t.maybeNull(User),
    // manager: t.maybeNull(t.reference(Manager)),
    // authenticating: t.optional(t.boolean, false),
    // auth_checking: t.optional(t.boolean, false),
    // token: t.maybeNull(t.string),
    // app_is_ready: t.optional(t.boolean, false),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const fetchItemsFromStorage = flow(function* () {
      self.setFavorites(yield storage.get('favorites', [])).then();
      self.root.categoryStore.createOrUpdate(yield storage.get('categories', []));
      self.setRegions(yield storage.get('regions', [])).then();
      self.setRegion(yield storage.get('region', null)).then();

      self.is_ready = true;
    });

    const toggleFavorite = (id, type, payload) => {
      if (self.in_favorite(id, type)) {
        self.removeFavorite(id, type);
      } else {
        self.addFavorite(id, type, toJS(payload));
      }
    };

    const regionDismiss = () => {
      self.region_show && self.setValue('region_show', false);
    };

    const removeFavorite = (id, type) => {
      self.favorites.splice(self.favorites.findIndex(v => v.id === id && v.type === type), 1);
      storage.set('favorites', self.favorites).then();
    };

    const addFavorite = (id, type, payload) => {
      self.favorites = [{id, type, payload}, ...self.favorites];
      storage.set('favorites', self.favorites).then();
    };

    // const setCategories = async (c, s = false) => {
    //   self.categories = c.map(v => {
    //     if (v instanceof ICategory) {
    //       return v;
    //     }
    //     return new ICategory(v);
    //   });
    //   if (s) {
    //
    //   }
    // };

    const setFavorites = flow(function*(c, s = false) {
      self.favorites = c;

      c.map(v => {
        if (v.type === 'store') {
          self.root.storeStore.createOrUpdate(v.payload);
        }
        else if(v.type === 'product'){
          self.root.productStore.createOrUpdate(v.payload);
        }
        else if(v.type === 'offer'){
          self.root.offerStore.createOrUpdate(v.payload);
        }
      });

      if (s) yield storage.set('favorites', c);
    });

    const setRegions = flow(function*(c, s = false) {
      // this.regions = c.map(v => {
      //   if (v instanceof ICategory) return v;
      //   return new ICategory(v);
      // });
      self.regions = c;
      if (s) {
        yield storage.set('regions', c);
      }
    });

    const setRegion = flow(function*(c, s = false) {
      self.setValue('region_show', false);
      self.region = c;
      if (s) {
        yield storage.set('region', c);
      }
    });

    const setLocation = (latitude, longitude) => {
      self.location_lat = latitude;
      self.location_lng = longitude;
    };

    // const signOut = () => {
    //   self.user = null;
    //   self.store = null;
    //   self.branch = null;
    //   self.auth_checking = false;
    //   self.authenticating = false;
    //   self.token = null;
    //   self.manager = null;
    //
    //   self.root.branchStore.clearItems();
    //   self.root.colorStore.clearItems();
    //   self.root.managerStore.clearItems();
    //   self.root.offerStore.clearItems();
    //   self.root.productStore.clearItems();
    //   self.root.userStore.clearItems();
    //
    //   storage.remove('token').then();
    //   storage.remove('manager').then();
    //   storage.remove('branch').then();
    //   storage.remove('store').then();
    //   storage.remove('user').then();
    // };
    //
    // const signIn = ({user, store, branch, token, manager, categories}) => {
    //   self.root.categoryStore.createOrUpdate(categories);
    //   self.root.managerStore.createOrUpdate(manager);
    //   if (branch) self.root.branchStore.createOrUpdate(branch);
    //
    //   self.user = user;
    //   self.store = store;
    //   self.branch = branch ? branch.id : null;
    //   self.token = token;
    //   self.manager = manager.id;
    //
    //   storage.set('user', user).then();
    //   storage.set('store', store).then();
    //   storage.set('branch', branch).then();
    //   storage.set('token', token).then();
    //   storage.set('manager', manager).then();
    //   storage.set('categories', categories).then();
    //   //storage.set('fcm_token', getSnapshot(self.store)).then();
    // };
    //
    // const checkAuth = () => {
    //   if (!self.token || self.auth_checking) {
    //     return;
    //   }
    //   self.setValue('auth_checking', true);
    //
    //   requester.get('/user/basic').then(response => {
    //     self.root.categoryStore.createOrUpdate(response.data.categories);
    //     self.root.managerStore.createOrUpdate(response.data.manager);
    //     if (response.data.branch) self.root.branchStore.createOrUpdate(response.data.branch);
    //
    //     self.setValue('store', response.data.store);
    //     if (response.data.branch) self.setValue('branch', response.data.branch.id);
    //     self.setValue('manager', response.data.manager.id);
    //     self.setValue('user', response.data.user);
    //   }).catch(e => {
    //     console.log(e);
    //     self.signOut();
    //   }).finally(() => {
    //     self.setValue('auth_checking', false);
    //   });
    // };

    return {
      setValue,
      fetchItemsFromStorage,
      toggleFavorite,
      regionDismiss,
      removeFavorite,
      addFavorite,
      setFavorites,
      setRegion,
      setRegions,
      setLocation,
      // signOut,
      // signIn,
      // checkAuth,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

    get favorites_products() {
      return self.favorites.filter(v => v.type === 'product'|| v.type === 'offer');
    },

    get favorites_stores() {
      return self.favorites.filter(v => v.type === 'store');
    },

    in_favorite(id, type) {
      return self.favorites.find(v => v.id === id && v.type === type);
    },

  }));
