import {types as t} from 'mobx-state-tree';
import AppStore from './AppStore';
import CategoryStore from './CategoryStore';
// import UserStore from './UserStore';
// import BranchStore from './BranchStore';
// import ManagerStore from './ManagerStore';
import OfferStore from './OfferStore';
import ProductStore from './ProductStore';
import BrandStore from './BrandStore';
import StoreStore from './StoreStore';
// import RegionStore from './RegionStore';
// import ColorStore from './ColorStore';

export default t
  .model('RootStore', {

    appStore: AppStore,
    categoryStore: CategoryStore,
    // userStore: UserStore,
    // branchStore: BranchStore,
    // managerStore: ManagerStore,
    offerStore: OfferStore,
    productStore: ProductStore,
    brandStore: BrandStore,
    // regionStore: RegionStore,
    // colorStore: ColorStore,
    storeStore: StoreStore,

  })
  .actions(self => {

    const setDataFromStorage = (values) => {
      // if (values[0]) self.appStore.token = values[0];
      // if (values[1]) self.appStore.store = values[1];
      // if (values[2]) self.appStore.fcm_token = values[2];
      // self.categoryStore.createOrUpdate(Object.values(values[3]));
      // if (values[4]) {
      //   self.branchStore.createOrUpdate(values[4]);
      //   self.appStore.branch = values[4].id;
      // }
      // if (values[5]) {
      //   self.managerStore.createOrUpdate(values[5]);
      //   self.appStore.manager = values[5].id;
      // }
      // if (values[6]) self.appStore.user = values[6];
      //
      // self.appStore.app_is_ready = true;
    };

    return {
      setDataFromStorage,
    };

  })
  .create({

    appStore: AppStore.create(),
    categoryStore: CategoryStore.create(),
    // userStore: UserStore.create(),
    // branchStore: BranchStore.create(),
    // managerStore: ManagerStore.create(),
    offerStore: OfferStore.create(),
    productStore: ProductStore.create(),
    brandStore: BrandStore.create(),
    storeStore: StoreStore.create(),

    // regionStore: RegionStore.create(),
    // colorStore: ColorStore.create(),

  });
