import {getRoot, types as t} from 'mobx-state-tree';
import Offer from '../models/MSTOffer';

export default t
  .model('OfferStore', {

    items: t.optional(t.map(Offer), {}),
    only_today: t.optional(t.array(t.reference(Offer)), []),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      if (item.product) self.root.productStore.createOrUpdate(item.product);

      self.items.set(item.id, {
        ...item,
        used: item.used === 1,
        product: item.product ? item.product.id : null,
        discount_active: item.discount_active === 1,
      });
    };

    return {
      setValue,
      createOrUpdate,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

  }));
