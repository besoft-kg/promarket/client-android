import {types as t} from 'mobx-state-tree';
import Brand from '../models/MSTBrand';

export default t
  .model('BrandStore', {

    items: t.optional(t.map(Brand), {}),
    popular: t.optional(t.array(t.reference(Brand)), []),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      self.items.set(item.id, {
        ...item,
      });
    };

    return {
      setValue,
      createOrUpdate,
    };

  })
  .views(self => ({



  }));
