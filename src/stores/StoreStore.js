import {getRoot, types as t} from 'mobx-state-tree';
import Store from '../models/MSTStore';
import {toJS} from 'mobx';

export default t
  .model('StoreStore', {
    items: t.optional(t.map(Store), {}),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      item = {...(self.items.has(item.id) ? toJS(self.items.get(item.id)) : {}), ...item};

      if ('offers' in item) item.offers.map(v => self.root.offerStore.createOrUpdate(v));

      self.items.set(item.id, {
        ...item,
        offers: (item.offers || []).map(v => v.id),
      });
    };

    return {
      setValue,
      createOrUpdate,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

  }));
