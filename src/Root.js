import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {Provider} from 'mobx-react';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import App from "./App";
import RootStore from './stores/RootStore';

const theme = {
  ...DefaultTheme,
};

const config = {
  screens: {
    Main: {
      screens: {
        Home: {
          screens: {
            ProductScreen: '/product/:id',
            StoreScreen: '/store/:id',
            OfferScreen: '/offer/:id'
          }
        }
      }
    }
  },
};

const linking = {
  prefixes: ['https://promarket.kg', 'http://promarket.kg', 'promarket://'],
  config,
};

const Root: () => React$Node = () => {
  return (
    <PaperProvider theme={theme}>
      <Provider store={RootStore}>
        <NavigationContainer linking={linking}>
          <StatusBar barStyle="dark-content"/>
          <SafeAreaView flex={1}>
            <App/>
          </SafeAreaView>
        </NavigationContainer>
      </Provider>
    </PaperProvider>
  );
};

export default Root;
