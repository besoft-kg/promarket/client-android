import React from 'react';
import BaseComponent from '../components/BaseComponent';
import {StatusBar, Text, View, ScrollView} from 'react-native';
import {observer} from 'mobx-react';
import {action, observable} from 'mobx';
import {requester} from '../utils/settings';
import {Card, Title} from 'react-native-paper';

@observer
class ProductSpecificationsScreen extends BaseComponent {
  @observable items = [];
  @observable loading = false;

  @action fetchItem = () => {
    if (this.loading) {
      return;
    }
    this.setValue('loading', true);
    requester.get('/product/specification', {
      id: this.props.route.params.item.id,
    }, true).then((response) => {
      this.setValue('items', response.data);
      console.log(response.data);
    }).catch((e) => {
      console.log(e);
    }).finally(() => {
      this.setValue('loading', false);
    });
  };

  componentDidMount() {
    this.fetchItem();
  }

  render() {
    return (
      <>
        <StatusBar barStyle="light-content"/>
        <ScrollView>
          {this.items.map((v, i) => (
            <Card style={{marginTop: 8, marginHorizontal: 8, padding: 8, marginBottom: i === this.items.length - 1 ? 8 : 0}}>
              <Title>{v.title}</Title>
              {v.properties.map(g => (
                <View style={{marginVertical: 8}}>
                  <Text>{`${g.title}:  `}<Text style={{color: '#808080'}}>{g.value}</Text></Text>
                </View>
              ))}
            </Card>
          ))}
        </ScrollView>
      </>
    );
  }
}

export default ProductSpecificationsScreen;
