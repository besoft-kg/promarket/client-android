import React  from 'react';
import BaseScreen from './BaseScreen';
import { FlatList, Image, Text, View } from 'react-native';
import { Card } from 'react-native-paper';
import { OFFICE_API_URL, requester } from '../utils/settings';
import { inject, observer } from 'mobx-react';
import { sizeNormalizer } from '../utils';
import { observable, values } from 'mobx';

@inject('store') @observer
class BrandsScreen extends BaseScreen {
  @observable loading = false;

  fetchItems = () => {
    if (this.loading) return false;
    this.setValue('loading', true);
    requester.get('/brand', {}).then((response) => {
      this.brandStore.createOrUpdate(response.data);
    }).catch(e => {
      console.dir(e);
    }).finally(() => {
      this.setValue('loading', false);
    });
  };

  componentDidMount() {
    this.fetchItems();
  }

  render() {
    return (
      <>
        <View>
          <FlatList
            refreshing={this.loading}
            onRefresh={() => this.fetchItems()}
            numColumns={3}
            showsHorizontalScrollIndicator={false}
            keyExtractor={item => item.id.toString()}
            renderItem={({ item }) => (
              <>
                <Card
                  onPress={() => this.props.navigation.navigate('BrandScreen', { id: item.id })}
                  style={{ margin: 6, width: sizeNormalizer(123), padding: 5 }}
                >
                  <Image style={{ backgroundColor: '#fff', width: '100%', height: 80 }} resizeMode={'contain'}
                         source={{ uri: item.logo ? `${OFFICE_API_URL}/${item.logo.path.original}` : '' }} />
                  <Text style={{ textAlign: 'center', marginVertical: 12, marginBottom: 8 }}>
                    {item.title}
                  </Text>
                </Card>
              </>
            )}
            data={values(this.brandStore.items).slice()}
          />
        </View>
      </>
    );
  }
}

export default BrandsScreen;
