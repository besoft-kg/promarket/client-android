import React from 'react';
import BaseComponent from '../components/BaseComponent';
import CategoriesComponent from '../components/CategoriesComponent';
import {RefreshControl, ScrollView, View, StatusBar, TouchableOpacity, Image} from 'react-native';
import {APP_SECONDARY_COLOR, INTRO_PAGE_VERSION, requester} from '../utils/settings';
import {observable, values} from 'mobx';
import {inject, observer} from 'mobx-react';
import {Colors, Headline, Searchbar} from 'react-native-paper';
import PopularBrandsComponent from '../components/PopularBrandsComponent';
import PopularProductsComponent from '../components/PopularProductsComponent';
import DiscountsComponent from '../components/DiscountsComponent';
import SectionsComponent from '../components/SectionsComponent';
import OnlyTodayComponent from '../components/OnlyTodayComponent';
import {sizeNormalizer} from '../utils';
import ProductsByCategoriesComponent from '../components/ProductsByCategoriesComponent';
import storage from '../utils/storage';

@inject('store') @observer
class HomeScreen extends BaseComponent {
  @observable sections = observable.array([]);
  @observable only_today = observable.array([]);
  @observable loading = false;
  @observable intro_page_version = null;

  constructor(props) {
    super(props);

    storage.get('intro_page_version', 0).then((response) => {
      if (response < INTRO_PAGE_VERSION) {
        this.props.navigation.replace('IntroPageScreen');
      }
    });
  }

  fetchItem = () => {
    if (this.loading) {
      return;
    }
    this.setValue('loading', true);
    requester.get('/to/home', {}, true).then((response) => {

      storage.set('categories', response.data.categories).then();

      this.categoryStore.createOrUpdate(response.data.categories);
      this.productStore.createOrUpdate(response.data.products.popular);
      this.productStore.createOrUpdate(response.data.products.discount);
      response.data.products.by_categories.map(v => this.productStore.createOrUpdate(v.products));
      response.data.products.by_categories.map(v => this.categoryStore.items.get(v.id).setProducts(v.products.map(g => g.id)));

      this.productStore.setValue('popular', response.data.products.popular.map(v => v.id));
      this.productStore.setValue('discount', response.data.products.discount.map(v => v.id));
      this.offerStore.createOrUpdate(response.data.offers.only_today);

      this.brandStore.createOrUpdate(response.data.brands.popular);
      this.brandStore.setValue('popular', response.data.brands.popular.map(v => v.id));

      this.offerStore.setValue('only_today', response.data.offers.only_today.map(v => v.id));
      this.setValue('sections', response.data.sections);

    }).catch((e) => {
      console.log(e.response);
    }).finally(() => {
      this.setValue('loading', false);
    });
  };

  componentDidMount() {
    this.fetchItem();
    this.unsubsriber = this.props.navigation.addListener('focus', () => {
      StatusBar.setBarStyle('dark-content');
    });
  }

  componentWillUnmount() {
    this.unsubsriber && this.unsubsriber();
  }

  render() {
    return (
      <>
        <ScrollView
          style={{padding: 6, marginTop: StatusBar.currentHeight}}
          refreshControl={<RefreshControl refreshing={this.loading} onRefresh={() => this.fetchItem()}/>}
        >
          <View style={{marginVertical: 8, alignItems: 'center', flexDirection: 'row', justifyContent: 'center'}}>
            <Image style={{width: 25, height: 25, marginRight: 5}} resizeMode={'contain'}
                   source={require('../assets/images/logo-274-304.png')}/>
            <Headline style={{color: Colors.green700, fontWeight: 'bold', textAlign: 'center'}}>
              <Headline style={{color: APP_SECONDARY_COLOR, fontWeight: 'bold'}}>PRO</Headline>MARKET
            </Headline>
          </View>
          <TouchableOpacity activeOpacity={0.9} onPress={() => this.props.navigation.navigate('SearchScreen')}>
            <Searchbar
              editable={false}
              inputStyle={{borderBottomWidth: 0}}
              showSoftInputOnFocus={false}
              style={{margin: sizeNormalizer(4)}}
              placeholder="Что вы ищете..."
            />
          </TouchableOpacity>
          <CategoriesComponent items={values(this.categoryStore.items).filter(v => !v.parent_id && v.products.length > 0)}/>
          <PopularProductsComponent items={this.productStore.popular}/>
          <OnlyTodayComponent items={this.offerStore.only_today}/>
          <DiscountsComponent items={this.productStore.discount}/>
          <SectionsComponent items={this.sections}/>
          <ProductsByCategoriesComponent />
          <PopularBrandsComponent />
        </ScrollView>
      </>
    );
  }
}

export default HomeScreen;
