import React from 'react';
import BaseComponent from '../components/BaseComponent';
import {
  FlatList, Image, ScrollView, Linking, StatusBar, Text,
  TouchableOpacity, View, RefreshControl, Alert, Share,
} from 'react-native';
import {inject, observer} from 'mobx-react';
import {Button, Card, Dialog, Title, Portal, TextInput} from 'react-native-paper';
import {action, observable} from 'mobx';
import {APP_PRIMARY_COLOR, APP_SECONDARY_COLOR, OFFICE_API_URL, requester} from '../utils/settings';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import BranchItemComponent from '../components/BranchItemComponent';
import OfferItemComponent from '../components/OfferItemComponent';
import PhoneNumber from '../utils/PhoneNumber';

const Tab = createMaterialTopTabNavigator();

@observer
class ProductsTab extends BaseComponent {
  render() {
    return (
      <FlatList
        numColumns={2}
        renderItem={({item}) => (
          <View style={{width: '50%'}}>
            <OfferItemComponent inline={false} item={{...item, store: this.props.store}}/>
          </View>
        )}
        data={this.props.items}
      />
    );
  }
}

class BranchesTab extends BaseComponent {
  render() {
    return (
      <FlatList
        style={{margin: 8}}
        renderItem={({item}) => (
          <>
            <View>
              <BranchItemComponent item={{
                branch: item,
                store: this.props.store,
              }}/>
            </View>
          </>
        )}
        data={this.props.items}
      />
    );
  }
}

@inject('store') @observer
class StoreScreen extends BaseComponent {
  @observable loading = false;
  @observable phone_dialog = false;
  @observable about_dialog = false;
  @observable complain_dialog = false;
  @observable text = '';
  @observable phone_number = '';

  clean = () => {
    this.setValue('complain_dialog', false);
    this.setValue('text', '');
    this.setValue('phone_number', '');
  };

  share = async () => {
    try {
      const result = await Share.share({
        message: `https://promarket.kg/store/${this.item.id}`,
        title: `Магазин ${this.item.title}`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
        } else {
        }
      } else if (result.action === Share.dismissedAction) {
      }
    } catch (error) {
      alert(error.message);
    }
  };

  @action fetchItem = () => {
    if (this.loading) {
      return;
    }
    this.setValue('loading', true);
    requester.get('/store', {id: this.item.id}, true).then((response) => {
      this.storeStore.createOrUpdate(response.data);
      this.props.navigation.setOptions({
        title: this.item.title,
      });
    }).catch((e) => {
      console.log(e);
    }).finally(() => {
      this.setValue('loading', false);
    });
  };

  @action fetchPost = () => {
    if (!this.phone_number_object.isValid() || this.text.length <= 0) {
      return;
    }
    requester.post('/support', {
      text: this.text,
      phone_number: this.phone_number_object.getE164Format(),
    }, true).then((response) => {
      Alert.alert('Успешно', 'Жалоба отправлена!');
      this.clean();
    }).catch((e) => {
      console.log(e);
    });
  };

  componentDidMount() {
    this.fetchItem();
  }

  renderContact = (v) => {

    let value = v.value;
    let icon = 'phone';
    let link = '';

    switch (v.type) {
      case 'whatsapp':
        value = `+${v.value}`;
        icon = 'whatsapp';
        link = `http://api.whatsapp.com/send?phone=${v.value}`;
        break;
      case 'phone':
        value = `+${v.value}`;
        icon = 'phone';
        link = `tel:+${v.value}`;
        break;
      case 'telegram':
        value = `${v.value}`;
        icon = 'telegram';
        link = `https://t.me/${v.value}`;
        break;
      case 'instagram':
        value = `${v.value}`;
        icon = 'instagram';
        link = `https://www.instagram.com/${v.value}`;
        break;
    }

    return (
      <TouchableOpacity
        onPress={() => Linking.openURL(link)}
        style={{flexDirection: 'row', alignItems: 'center', marginBottom: 8}}
      >
        <Icon size={20} name={icon}/>
        <Title style={{marginLeft: 8}}>{value}</Title>
      </TouchableOpacity>
    );
  };

  constructor(props) {
    super(props);

    this.item = this.storeStore.items.get(this.props.route.params.id);
    this.phone_number_object = new PhoneNumber();
  }

  setPhoneNumber = n => {
    this.phone_number_object.parseFromString(n);
    this.setValue('phone_number', n);
  };

  render() {
    const in_favorite = this.appStore.favorites.find(v => v.id === this.item.id && v.type === 'store');

    return (
      <>
        <StatusBar barStyle="dark-content"/>
        <ScrollView
          style={{marginTop: StatusBar.currentHeight}}
          refreshControl={<RefreshControl refreshing={this.loading} onRefresh={() => this.fetchItem()}/>}
        >
          <Card style={{
            flex: 1,
            flexDirection: 'row',
            paddingTop: StatusBar.currentHeight,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            {this.item.logo && (
              <Image style={{width: 100, height: 100, alignSelf: 'center'}} resizeMode={'contain'}
                     source={{uri: `${OFFICE_API_URL}/${this.item.logo.path.original}`}}/>
            )}
            <Text style={{
              fontSize: 24,
              marginTop: this.item.logo ? 0 : 8,
              textAlign: 'center',
              marginBottom: 8,
            }}>{this.item.title}</Text>
          </Card>
          <View style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-around',
            marginTop: 12,
            marginBottom: 8,
          }}>
            <TouchableOpacity onPress={() => this.setValue('phone_dialog', true)}>
              <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
                <Icon size={40} name={'phone'}/>
                <Text>Связаться</Text>
              </View>
            </TouchableOpacity>
            <View style={{height: '70%', width: 1, backgroundColor: '#DCDCDC'}}/>
            <TouchableOpacity onPress={() => this.setValue('about_dialog', true)}>
              <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
                <Icon size={40} name={'newspaper-variant'}/>
                <Text>О магазине</Text>
              </View>
            </TouchableOpacity>
            <View style={{height: '70%', width: 1, backgroundColor: '#DCDCDC'}}/>
            <TouchableOpacity onPress={() => this.setValue('complain_dialog', true)}>
              <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
                <Icon size={40} name={'alert-octagon'}/>
                <Text>Пожаловаться</Text>
              </View>
            </TouchableOpacity>
            <View style={{height: '70%', width: 1, backgroundColor: '#DCDCDC'}}/>
            <TouchableOpacity onPress={() => this.share()}>
              <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
                <Icon size={40} name={'share-variant'}/>
                <Text>Поделиться</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View>
            <Button style={{marginHorizontal: 5, marginVertical: 5}}
                    color={in_favorite ? APP_SECONDARY_COLOR : APP_PRIMARY_COLOR}
                    icon={in_favorite ? 'heart' : 'heart-plus-outline'} mode="contained"
                    onPress={() => this.appStore.toggleFavorite(this.item.id, 'store', this.item)}>
              {in_favorite ? 'Удалить из избранных' : 'Сохранить в избранные'}
            </Button>
          </View>

          <Tab.Navigator backBehavior={'none'}>
            <Tab.Screen name="Товары">
              {(props) => (
                <ProductsTab {...props} store={{title: this.item.title}} items={this.item.offers}/>
              )}
            </Tab.Screen>
            <Tab.Screen name="Филиалы">
              {(props) => (
                <BranchesTab {...props} items={this.item.branches} store={{logo: this.item.logo}}/>
              )}
            </Tab.Screen>
          </Tab.Navigator>
        </ScrollView>

        <Portal>
          <Dialog visible={this.phone_dialog} onDismiss={() => this.setValue('phone_dialog', false)}>
            <Dialog.Content>
              {(this.item.contacts || []).length > 0 ? (
                <>
                  {(this.item.contacts || []).map((v, i) => this.renderContact(v))}
                </>
              ) : (
                <Title>Нет контактов</Title>
              )}
            </Dialog.Content>
            <Dialog.Actions>
              <Button onPress={() => this.setValue('phone_dialog', false)}>Закрыть</Button>
            </Dialog.Actions>
          </Dialog>

          <Dialog style={{marginVertical: 130}} visible={this.about_dialog}
                  onDismiss={() => this.setValue('about_dialog', false)}>
            <Dialog.Title>Магазин {this.item.title}</Dialog.Title>
            <Dialog.ScrollArea>
              <ScrollView style={{marginVertical: 8}}>
                <Text>{this.item.about}</Text>
              </ScrollView>
            </Dialog.ScrollArea>
            <Dialog.Actions>
              <Button onPress={() => this.setValue('about_dialog', false)}>Закрыть</Button>
            </Dialog.Actions>
          </Dialog>

          <Dialog visible={this.complain_dialog} onDismiss={() => this.setValue('complain_dialog', false)}>
            <View style={{margin: 8}}>
              <Title>Уважаемые покупатели!</Title>
              {/* <Text>Если Вы купили некачественный товар в нашем магазине, Вы можете вернуть товар, либо обменять его на
                  качественный. Для этого Вам необходимо предъявить товар и чек администратору магазина.
                </Text>*/}
              <Text>Если Вы купили некачественный товар в нашем магазине,
                сообщите об этом. Ваше сообщение получат магазин и Админимстрация ProMarket!
              </Text>
              <TextInput
                label="Сообщить о проблеме"
                multiline
                value={this.text}
                onChangeText={(n) => this.setValue('text', n)}
              />
              <TextInput
                keyboardType="phone-pad"
                label="Номер телефона"
                value={this.phone_number}
                onChangeText={(n) => this.setPhoneNumber(n)}
              /></View>
            <Dialog.Actions>
              <Button disabled={!this.phone_number_object.isValid() || this.text.length <= 0}
                      onPress={() => this.fetchPost()}>Отправить</Button>
              <Button onPress={() => this.clean()}>Закрыть</Button>
            </Dialog.Actions>
          </Dialog>

        </Portal>
      </>
    );
  }
}

export default StoreScreen;
