import React from 'react';
import BaseComponent from '../components/BaseComponent';
import {Card, Text, Title, List, ActivityIndicator} from 'react-native-paper';
import {Image, View, ScrollView, TouchableOpacity, RefreshControl, StatusBar, Linking, Share} from 'react-native';
import {inject, observer} from 'mobx-react';
import {action, comparer, observable} from 'mobx';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {OFFICE_API_URL, requester} from '../utils/settings';
import {sizeNormalizer, windowSize} from '../utils';
import ProductOfferItemComponent from '../components/ProductOfferItemComponent';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {Button, Dialog, Portal} from 'react-native-paper';
import {HeaderButton, HeaderButtons, Item} from 'react-navigation-header-buttons';
import MaterialIcons from 'react-native-vector-icons/MaterialCommunityIcons';

@inject('store') @observer
class ProductScreen extends BaseComponent {
  @observable item = null;
  @observable loading = false;
  @observable active_image = 0;
  @observable selected_color = null;
  @observable selected_modifications = {};
  @observable article_dialog = false;

  carousel_ref = React.createRef();

  constructor(props) {
    super(props);

    this.item = this.productStore.items.get(this.props.route.params.id);

    this.props.navigation.setOptions({
      title: this.item ? this.item.title : 'Подождите...',
      headerRight: () => (
        <HeaderButtons
          HeaderButtonComponent={props => (
            <HeaderButton IconComponent={MaterialIcons} iconSize={23} color="#fff" {...props} />
          )}>
          <Item title={'plus'} onPress={() => this.share()} iconName={'share'}/>
        </HeaderButtons>
      ),
    });
  }

  @action fetchItem = () => {
    if (this.loading) {
      return;
    }

    const id = this.item ? this.item.id : 'id' in this.props.route.params ? this.props.route.params.id : this.props.route.params.item.id;

    this.setValue('loading', true);
    requester.get('/product', {
      id,
    }, true).then((response) => {

      this.productStore.createOrUpdate(response.data);
      this.setValue('item', this.productStore.items.get(id));

      this.props.navigation.setOptions({
        title: this.item.title,
      });

    }).catch((e) => {
      console.dir(e);
    }).finally(() => {
      this.setValue('loading', false);
    });
  };

  componentDidMount() {
    this.fetchItem();
  }

  share = async () => {
    if (!this.item) return;
    try {
      const result = await Share.share({
        message: `https://promarket.kg/product/${this.item.id}`,
        title: `${this.item.title}`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
        } else {
        }
      } else if (result.action === Share.dismissedAction) {
      }
    } catch (error) {
      alert(error.message);
    }
  };

  @action setSelectedColor = (v) => {
    this.selected_color = this.selected_color !== v.id ? v.id : null;
    this.active_image = 0;
    this.carousel_ref.snapToItem(0);
  };

  @action selectModification(v, g) {
    if (v.id in this.selected_modifications && comparer.structural(this.selected_modifications[v.id], g)) {
      delete this.selected_modifications[v.id];
    } else {
      this.selected_modifications[v.id] = g;
    }
  }

  modificationRender = (v) => {
    let options = v.options ? v.options : [{id: 1, value: 'Да'}, {id: 2, value: 'Нет'}];
    let selected_value = 'не важно';

    if (v.id in this.selected_modifications) {
      selected_value = typeof this.selected_modifications[v.id] === 'object' ? this.selected_modifications[v.id].value : `${this.selected_modifications[v.id]}${v.suffix ? ` ${v.suffix}` : null}`;
    }

    return (
      <View style={{margin: 8}}>
        <Text>{v.title}: {selected_value}</Text>
        <View flexDirection={'row'} style={{marginTop: 4, flexWrap: 'wrap'}}>
          {options.map(g => (
            <TouchableOpacity
              onPress={() => this.selectModification(v, g)}
              style={{
                minWidth: 60,
                padding: 6,
                marginRight: 4,
                marginBottom: 4,
                backgroundColor: '#fff',
                borderWidth: 1,
                borderRadius: 10,
                borderColor: comparer.structural(this.selected_modifications[v.id], g) ? '#808080' : '#eee',
              }}>
              <Text
                style={{textAlign: 'center'}}>{typeof g === 'object' ? g.value : g}{v.suffix ? ` ${v.suffix}` : null}</Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    );
  };

  render() {
    if (!this.item) {
      return (
        <>
          <StatusBar barStyle="light-content"/>
          <View flex={1} justifyContent={'center'} alignItems={'center'}>
            <ActivityIndicator/>
          </View>
        </>
      );
    }

    const filtered_images = this.item.images.filter(v => (v.color_id === this.selected_color) || !this.selected_color);
    return (
      <>
        <StatusBar barStyle="light-content"/>
        <ScrollView refreshControl={<RefreshControl refreshing={this.loading} onRefresh={() => this.fetchItem()}/>}>
          <View style={{alignItems: 'center', paddingVertical: 16, backgroundColor: '#fff'}}>
            <Carousel
              ref={ref => this.carousel_ref = ref}
              containerCustomStyle={{marginBottom: 10}}
              layout={'default'}
              data={this.item.images.length > 0 ? filtered_images : (this.item.main_image ? [{image: this.item.main_image}] : null)}
              sliderWidth={windowSize.width}
              itemWidth={windowSize.width}
              onSnapToItem={index => this.setValue('active_image', index)}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductImageModal', {
                  item: this.item.images.length > 0 ? filtered_images : (this.item.main_image ? [{image: this.item.main_image}] : null),
                  index: this.active_image,
                })} style={{alignItems: 'center'}}>
                  <Image style={{backgroundColor: '#fff', width: '100%', height: 150}} resizeMode={'contain'}
                         source={{uri: `${OFFICE_API_URL}/${item.image.path.original}`}}/>
                </TouchableOpacity>
              )}
            />
            <Pagination
              dotsLength={filtered_images.length}
              activeDotIndex={this.active_image}
              containerStyle={{marginVertical: -20}}
              dotStyle={{
                width: 6,
                height: 6,
                marginHorizontal: -5,
                backgroundColor: 'rgba(81,187,17,0.99)',
              }}
            />
          </View>

          <View style={{margin: 8}}>
            <Title>{this.item.title}</Title>
          </View>

          <View style={{
            flex: 1,
            flexDirection: 'row',
            marginHorizontal: 8,
            marginBottom: this.item.modifications.length === 0 ? 8 : 0,
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
            <View flexDirection={'row'}>
              {this.item.colors.map(v => (
                <TouchableOpacity onPress={() => this.setSelectedColor(v)} style={{
                  marginRight: 3,
                  borderRadius: 30,
                  borderWidth: this.selected_color === v.id ? 3 : 1,
                  borderColor: '#fff',
                  width: 30,
                  height: 30,
                  backgroundColor: `#${v.hex_code}`,
                }}/>
              ))}
            </View>

            {this.item.brand && (
              <View>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('BrandScreen', {id: this.item.brand.id})}>
                  {this.item.brand.logo ? (
                    <Image style={{width: 40, height: 40}} resizeMode={'contain'}
                           source={{uri: `${OFFICE_API_URL}/${this.item.brand.logo.path.original}`}}/>
                  ) : (
                    <Text style={{textAlign: 'right'}}>{this.item.brand.title}</Text>
                  )}
                </TouchableOpacity>
              </View>
            )}
          </View>

          {this.item.modifications.length > 0 && (
            <>
              {this.item.modifications.map(v => this.modificationRender(v))}
            </>
          )}

          {/*{this.item.cheapest_offer && this.item.cheapest_offer.store && (*/}
          {/*  <ProductOfferItemComponent item={this.item.cheapest_offer} title={'Самая низкая цена'}/>*/}
          {/*)}*/}

          <View>
            {this.item.offers_count > 0 ? (
              <>
                <View style={{
                  paddingHorizontal: 8,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  flexDirection: 'row',
                }}>
                  <Title style={{fontSize: 14}}>Предложения ({this.item.offers_count})</Title>
                </View>

                {this.item.offers.length > 0 && (
                  <>
                    {this.item.offers.map(v => (
                      <ProductOfferItemComponent productTitle={this.item.title === v.product.title ? null : v.product.title} item={v}/>
                    ))}
                  </>
                )}
              </>
            ) : (
              <>
                <Card style={{margin: 8, padding: 8, alignItems: 'center'}}>
                  <Title style={{color: '#808080', textAlign: 'center'}}>Нет в продаже</Title>
                  <Text style={{color: '#808080', textAlign: 'center'}}>
                    Добавьте в избранные чтобы получить уведомление о появлении товара в магазинах...
                  </Text>
                  <Button onPress={() => this.appStore.toggleFavorite(this.item.id, 'product', this.item)}
                          mode={'outlined'} style={{marginTop: 8}}>
                    {this.appStore.in_favorite(this.item.id, 'product') ? 'Убрать из избранных' : 'Добавить в избранные'}
                  </Button>
                </Card>
              </>
            )}
          </View>

          {this.item.brief_info.length > 0 && (
            <Card style={{paddingVertical: sizeNormalizer(8), marginBottom: sizeNormalizer(8), marginHorizontal: sizeNormalizer(8)}}>
              <View style={{marginLeft: sizeNormalizer(8)}}>
                <Title style={{fontSize: sizeNormalizer(14)}}>Коротко о товаре</Title>
                {this.item.brief_info.map(v => (
                  <>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Icon style={{color: '#32CD32', paddingRight: sizeNormalizer(5)}} size={sizeNormalizer(8)} name={'checkbox-blank-circle'}/>
                      <Text>{v.value}</Text>
                    </View>
                  </>
                ))}
                {/*<Divider style={{marginTop: 5}}/>*/}
                {/*<TouchableOpacity*/}
                {/*  onPress={() => this.props.navigation.navigate('ProductSpecificationsScreen', {item: this.item})}*/}
                {/*  style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}*/}
                {/*>*/}
                {/*  <Title style={{fontSize: 12}}>Все характеристики</Title>*/}
                {/*  <Icon style={{paddingRight: 5}} size={20} name={'chevron-right'}/>*/}
                {/*</TouchableOpacity>*/}
              </View>
            </Card>
          )}

          {this.item.articles.length > 0 && (
            <Card style={{padding: sizeNormalizer(8), marginHorizontal: sizeNormalizer(8), marginBottom: sizeNormalizer(8)}}
                  onPress={() => this.setValue('article_dialog', true)}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <Text style={{flex: 1, justifyContent: 'flex-end', marginTop: sizeNormalizer(5), flexDirection: 'column'}}>
                  Обзоры
                  ({this.item.articles.length})</Text>
                <Icon color={'red'} size={sizeNormalizer(30)} name="youtube"/>
              </View>
            </Card>
          )}

          {this.item.articles.length > 0 && (
            <Portal>
              <Dialog visible={this.article_dialog} onDismiss={() => this.setValue('article_dialog', false)}>
                {this.item.articles.map(v => (
                  <List.Item
                    titleStyle={{color: '#33A5DB', marginVertical: -10}}
                    onPress={() => {
                      Linking.openURL(v.url);
                    }} title={v.title}
                    left={props =>
                      <View justifyContent={'center'}
                            style={{...props, marginLeft: -15, marginRight: -15, marginVertical: -10}}>
                        <List.Icon color={'red'} icon="youtube"/>
                      </View>
                    }/>
                ))}
                <Dialog.Actions>
                  <Button onPress={() => this.setValue('article_dialog', false)}>Закрыть</Button>
                </Dialog.Actions>
              </Dialog>
            </Portal>
          )}
        </ScrollView>
      </>
    );
  }
}

export default ProductScreen;
