import React from 'react';
import {Button, Card, Colors, Text, Title} from 'react-native-paper';
import {View, StatusBar, FlatList, Image, Share} from 'react-native';
import {inject, observer} from 'mobx-react';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import ProductItemComponent from '../components/ProductItemComponent';
import OfferItemComponent from '../components/OfferItemComponent';
import BaseComponent from '../components/BaseComponent';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {sizeNormalizer} from '../utils';
import {OFFICE_API_URL} from '../utils/settings';

const Tab = createMaterialTopTabNavigator();

@inject('store') @observer
class ProductsTab extends BaseComponent {
  render() {
    return (
      <FlatList
        numColumns={2}
        renderItem={({item}) => (
          <>
            {item.type === 'product' ? (
              <View style={{width: '50%'}}>
                <ProductItemComponent inline={false} item={item.payload}/>
              </View>
            ) : null}
            {item.type === 'offer' ? (
              <View style={{width: '50%'}}>
                <OfferItemComponent inline={false} item={item.payload}/>
              </View>
            ) : null}
          </>
        )}
        ListEmptyComponent={() => (
          <View style={{marginVertical: 100, alignItems: 'center'}} alignItems={'center'}>
            <Image source={require('../assets/images/empty_favorites.png')}/>
            <Title style={{marginVertical: 8, color: Colors.grey800}}>В избранном пока ничего нет</Title>
            <Text style={{
              textAlign: 'center',
              fontSize: 16,
              color: Colors.grey700,
            }}> {`Добавляйте товары в Избранные \n с помощью`} <Icon name="heart" color="red" size={20}/></Text>
            <Button style={{marginTop: 24}} color={Colors.green600} icon="home" mode="outlined"
                    onPress={() => this.props.navigation.navigate('HomeScreen')}>На главную</Button>
          </View>
        )}
        data={this.appStore.favorites_products}
      />
    );
  }
}

@inject('store') @observer
class StoreTab extends BaseComponent {

  share = async (item) => {
    try {
      const result = await Share.share({
        message: `https://promarket.kg/store/${item.id}`,
        title: `Магазин ${item.title}`,
      });
      // if (result.action === Share.sharedAction) {
      //   if (result.activityType) {
      //   } else {
      //   }
      // } else if (result.action === Share.dismissedAction) {
      // }
    } catch (error) {
      alert(error.message);
    }
  };

  render() {
    const in_favorite = this.appStore.favorites.find(v => this.appStore.favorites_stores.id === v.id &&  v.type === 'store');

    return (
      <FlatList
        numColumns={2}
        style={{margin: 8}}
        renderItem={({item}) => (
          <View style={{margin: sizeNormalizer(2),flex: 1}} >
            <Card elevation={1} style={{width: sizeNormalizer(165), margin: 5, flex: 1, justifyContent: 'space-between'}} onPress={() => this.props.navigation.navigate('StoreScreen', {id: item.id})}>
              <View style={{paddingHorizontal: sizeNormalizer(6)}}>
                {item.payload.logo && (
                  <Image style={{width: 100, height: 100, alignSelf: 'center'}} resizeMode={'contain'}
                         source={{uri: `${OFFICE_API_URL}/${item.payload.logo.path.original}`}}/>
                )}
                <Title style={{alignSelf: 'center', marginVertical: 3}}>
                  {item.payload.title}
                </Title>
                <View style={{
                  alignItems: 'flex-end',
                  justifyContent: 'flex-end',
                  flexDirection: 'row',
                }}>
                  <View style={{flexDirection: 'row'}}>
                    <Icon
                      name="share-variant"
                      color={'#808080'}
                      size={25}
                      onPress={() => this.share(item)}
                    />
                    <Icon style={{paddingLeft: 8}} size={27} color={!in_favorite ? Colors.red500 : '#808080'}
                          onPress={() => this.appStore.toggleFavorite(item.id, 'store', this.item)}
                          name={!in_favorite ? 'heart' : 'heart-plus-outline'}/>
                  </View>
                </View>
              </View>
            </Card>
          </View>
        )}
        ListEmptyComponent={() => (
          <View style={{marginVertical: 100, alignItems: 'center'}} alignItems={'center'}>
            <Image source={require('../assets/images/empty_favorites.png')}/>
            <Title style={{marginVertical: 8, color: Colors.grey800}}>В избранном пока ничего нет</Title>
            <Text style={{
              textAlign: 'center',
              fontSize: 16,
              color: Colors.grey700,
            }}> {`Сохраняйте магазины в Избранные \n с помощью`} <Icon name="heart" color="red" size={20}/></Text>
            <Button style={{marginTop: 24}} color={Colors.green600} icon="home" mode="outlined"
                    onPress={() => this.props.navigation.navigate('HomeScreen')}>На главную</Button>
          </View>
        )}
        data={this.appStore.favorites_stores}
      />
    );
  }
}

@inject('store') @observer
class FavoritesScreen extends BaseComponent {
  componentDidMount() {
    this.unsubsriber = this.props.navigation.addListener('focus', () => {
      StatusBar.setBarStyle('light-content');
    });
  }

  componentWillUnmount() {
    this.unsubsriber && this.unsubsriber();
  }

  render() {
    return (
      <Tab.Navigator backBehavior={'none'}>
        <Tab.Screen name="Товары">
          {(props) => (
            <ProductsTab {...props}/>
          )}
        </Tab.Screen>
        <Tab.Screen name="магазины">
          {(props) => (
            <StoreTab {...props}/>
          )}
        </Tab.Screen>
      </Tab.Navigator>
    );
  }
}

export default FavoritesScreen;
