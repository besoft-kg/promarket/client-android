import React from 'react';
import BaseScreen from './BaseScreen';
import {Image, StatusBar, View} from 'react-native';
import {observable} from 'mobx';
import {inject, observer} from 'mobx-react';
import {FAB, Text} from 'react-native-paper';
import ViewPager from '@react-native-community/viewpager';
import storage from '../utils/storage';
import {INTRO_PAGE_VERSION} from '../utils/settings';

const logo = require('../assets/images/logo-274-304.png');

@inject('store') @observer
class IntroPageScreen extends BaseScreen {
  @observable current_page = 0;

  view_pager_ref = React.createRef();

  hide = () => {
    storage.set('intro_page_version', INTRO_PAGE_VERSION).then();
    this.props.navigation.replace('Main', {screen: 'Home'});
  };

  nextPage = () => {
    this.view_pager_ref.setPage(this.current_page + 1);
    if ((this.current_page + 1) === 3) {
      this.hide();
    }
  };

  render() {
    return (
      <>
        <StatusBar translucent={true} backgroundColor={'rgba(0, 0, 0, 0)'}/>

        <View flex={1}>
          <ViewPager
            initialPage={this.current_page}
            ref={ref => this.view_pager_ref = ref}
            onPageSelected={(i) => this.setValue('current_page', i.nativeEvent.position)}
            flex={1}
          >
            <View style={{
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#5a9216',
              padding: 18,
            }} key="1">
              <View style={{
                backgroundColor: 'rgba(255, 255, 255, 0.5)',
                borderRadius: 100,
                padding: 22,
                borderWidth: 1,
                borderColor: '#fff',
              }}>
                <Image style={{width: 150, height: 150}} resizeMode={'contain'} source={logo}/>
              </View>
              <Text style={{textAlign: 'center', fontSize: 24, marginVertical: 18, color: 'rgba(0, 0, 0, .55)'}}>ДОБРО
                ПОЖАЛОВАТЬ!</Text>
              <Text style={{textAlign: 'center', fontSize: 16, color: 'rgba(0, 0, 0, .79)'}}>ProMarket - это
                маркетплейс, где собраны все магазины Кыргызстана в одном приложении. Здесь вы можете найти все что искали, по выгодной
                цене и из разных магазинов.</Text>
            </View>
            {/*<View style={{*/}
            {/*  flexDirection: 'column',*/}
            {/*  alignItems: 'center',*/}
            {/*  justifyContent: 'center',*/}
            {/*  backgroundColor: APP_SECONDARY_COLOR,*/}
            {/*  padding: 18,*/}
            {/*}} key="2">*/}
            {/*  <View style={{backgroundColor: 'rgba(255, 255, 255, 0.5)', borderRadius: 100, padding: 22, borderWidth: 1, borderColor: '#fff'}}>*/}
            {/*    <Image style={{width: 150, height: 150}} resizeMode={'contain'} source={logo}/>*/}
            {/*  </View>*/}
            {/*  <Text style={{textAlign: 'center', fontSize: 24, marginVertical: 18, color: 'rgba(0, 0, 0, .55)'}}>СРАВНИВАЙТЕ ЦЕНЫ НА ТОВАРЫ</Text>*/}
            {/*  <Text style={{textAlign: 'center', fontSize: 16, color: 'rgba(0, 0, 0, .79)'}}>И выбирайте, где выгоднее купить то, что нужно. Вы можете увидеть предложения в своём городе и проверить условия доставки или расположение на карте.</Text>*/}
            {/*</View>*/}
            <View style={{
              margin: 8,
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#002984',
              padding: 18,
            }} key="3">
              <View style={{
                backgroundColor: 'rgba(255, 255, 255, 0.5)',
                borderRadius: 100,
                padding: 22,
                borderWidth: 1,
                borderColor: '#fff',
              }}>
                <Image style={{width: 150, height: 150}} resizeMode={'contain'} source={logo}/>
              </View>
              <Text style={{textAlign: 'center', fontSize: 24, marginVertical: 18, color: 'rgba(255, 255, 255, .55)'}}>НАХОДИТЕ
                ВЫГОДНЫЕ ПРЕДЛОЖЕНИЯ</Text>
              <Text style={{textAlign: 'center', fontSize: 16, color: 'rgba(255, 255, 255, .55)'}}>Все скидки реальные,
                а не накрученные. Сотни новых предложений каждый день: распродажи в магазинах и предложения брендов,
                подобранные специально для вас. Заходите в раздел скидок и выбирайте товар в нужной вам
                категории.</Text>
            </View>
            <View style={{
              margin: 8,
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#008ba3',
              padding: 18,
            }} key="4">
              <View style={{
                backgroundColor: 'rgba(255, 255, 255, 0.5)',
                borderRadius: 100,
                padding: 22,
                borderWidth: 1,
                borderColor: '#fff',
              }}>
                <Image style={{width: 150, height: 150}} resizeMode={'contain'} source={logo}/>
              </View>
              <Text style={{textAlign: 'center', fontSize: 24, marginVertical: 18, color: 'rgba(255, 255, 255, .77)'}}>ВЫБИРАЙТЕ
                СРЕДИ ТЫСЯЧИ ТОВАРОВ И БРЕНДОВ</Text>
              <Text style={{textAlign: 'center', fontSize: 16, color: 'rgba(255, 255, 255, .77)'}}>На ProMarket больше
                10 тыс товаров из 1000 магазинов в 300 категорий: телефоны, электроника, бытовая
                техника, {/*'корма для животных,'*/}одежда и обувь, автозапчасти, {/*'продукты'*/},
                игрушки{/*', лекарства'*/} и многое другое.</Text>
            </View>
          </ViewPager>
          <View style={{position: 'absolute', width: '100%', bottom: 14, left: 0}} alignItems={'center'}
                justifyContent={'center'}>
            <View flexDirection={'column'} style={{width: '100%'}} alignItems={'stretch'}>
              <View flexDirection={'row'} justifyContent={'center'} alignItems={'center'}>
                {[0, 1, 2].map(v => (
                  <View style={{
                    width: 10,
                    opacity: this.current_page === v ? 1 : .3,
                    height: 10,
                    margin: 4,
                    backgroundColor: '#fff',
                    borderRadius: 100,
                  }}/>
                ))}
              </View>
              <FAB
                style={{alignSelf: 'flex-end', marginRight: 12}}
                icon={this.current_page === 2 ? 'rocket' : 'arrow-right'}
                onPress={() => this.nextPage()}
              />
            </View>
          </View>
        </View>
      </>
    );
  }
}

export default IntroPageScreen;
