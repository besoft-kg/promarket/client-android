import React from 'react';
import {Card, Text} from 'react-native-paper';
import {ScrollView, StatusBar,} from 'react-native';
import {inject, observer} from "mobx-react";
import BaseScreen from "./BaseScreen";
import {sizeNormalizer} from '../utils';

@inject('store') @observer
class AboutUsScreen extends BaseScreen {
  render() {
    return (
      <>
        <StatusBar translucent backgroundColor="transparent" />
        <ScrollView style={{margin: sizeNormalizer(8)}}>
          <Card style={{padding: sizeNormalizer(8)}}>
            <Text>ProMarket - это
              маркетплейс, где собраны все магазины Кыргызстана в одном приложении. Здесь вы можете найти все что искали, по выгодной
              цене из разных магазинов.</Text>
          </Card>

          <Card style={{padding: sizeNormalizer(8), marginTop: sizeNormalizer(8)}}>
            <Text>Наши контакты: +996 777 171 171 WhatsApp, Telegram</Text>
            <Text>Наши Instagram: promarket.kg</Text>
          </Card>
        </ScrollView>
      </>
    );
  }
}

export default AboutUsScreen;
