import React from 'react';
import BaseComponent from '../components/BaseComponent';
import {FlatList, View} from 'react-native';
import { inject, observer } from 'mobx-react';
import {observable} from 'mobx';
import {requester} from '../utils/settings';
import ProductItemComponent from '../components/ProductItemComponent';

@inject('store') @observer
class BrandScreen extends BaseComponent {
  @observable item = null;
  @observable loading = false;

  constructor(props) {
    super(props);

    this.item = this.brandStore.items.get(this.props.route.params.id);

    this.props.navigation.setOptions({
      title: `Товары бренда ${this.item.title}`,
    });
  }

  fetchItem = () => {
    if (this.loading) return;
    this.setValue('loading', true);
    requester.get('/brand', {id: this.item.id}, true).then((response) => {
      this.productStore.createOrUpdate(response.data.products);
      this.brandStore.createOrUpdate({
        ...response.data,
        products: response.data.products.map(g => g.id),
      });
    }).catch((e) => {
      console.log(e);
    }).finally(() => {
      this.setValue('loading', false);
    });
  };

  componentDidMount() {
    this.fetchItem();
  }

  render() {
    return (
      <>
        <FlatList
          refreshing={this.loading}
          onRefresh={() => this.fetchItem()}
          numColumns={2}
          renderItem={({item}) => (
            <View style={{width: '50%'}}>
              <ProductItemComponent inline={false} item={item}/>
            </View>
          )}
          data={this.item.products}
        />
      </>
    );
  }
}

export default BrandScreen;
