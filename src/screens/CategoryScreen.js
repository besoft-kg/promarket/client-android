import React from 'react';
import {View, Image, FlatList, StatusBar} from 'react-native';
import {action, observable} from 'mobx';
import {inject, observer} from 'mobx-react';
import {Title} from 'react-native-paper';
import ProductItemComponent from '../components/ProductItemComponent';
import {OFFICE_API_URL, requester} from '../utils/settings';
import BaseScreen from './BaseScreen';
import {SvgUri} from 'react-native-svg';
import CategoriesComponent from '../components/CategoriesComponent';

@inject('store')
@observer
class CategoryScreen extends BaseScreen {
  @observable item = null;
  @observable loading = false;

  constructor(props) {
    super(props);

    this.item = this.categoryStore.items.get(this.props.route.params.item.id);

    this.props.navigation.setOptions({
      headerShown: false,
    });
  }

  @action fetchItem = () => {
    if (this.loading) {
      return;
    }
    this.setValue('loading', true);
    requester.get('/category', {id: this.item.id}, true).then((response) => {
      this.productStore.createOrUpdate(response.data.products);
      this.item.setProducts(response.data.products.map(v => v.id));
    }).catch((e) => {
      console.log(e);
    }).finally(() => {
      this.setValue('loading', false);
    });
  };

  componentDidUpdate(prevProps) {
    if (prevProps.route.params.item.id !== this.props.route.params.item.id) {
      this.fetchItem();
    }
  }

  componentDidMount() {
    this.fetchItem();
  }

  render() {
    return (
      <FlatList
        style={{marginTop: StatusBar.currentHeight}}
        ListHeaderComponent={(
          <>
            {this.item.icon && (
              <View alignItems={'center'} style={{marginBottom: 15, marginVertical: 15}}>
                {this.item.icon.extension === 'svg' ? (
                  <SvgUri
                    width={50}
                    height={50}
                    fill="black"
                    uri={`${OFFICE_API_URL}/${this.item.icon.path.original}`}
                  />
                ) : (
                  <Image
                    resizeMode={'contain'}
                    style={{width: 200, height: 100}}
                    source={{uri: `${OFFICE_API_URL}/${this.item.icon.path.original}`}}
                  />
                )}
              </View>
            )}
            <View style={{marginHorizontal: 8}}>
              <View alignItems={'center'} style={{marginBottom: 15}}>
                <Title style={{textAlign: 'center'}}>{this.item.title}</Title>
              </View>
              <CategoriesComponent items={this.item.subcategories}
                                   onSelectItem={item => this.props.navigation.push(`CategoryScreen`, {item})}/>
            </View>
          </>
        )}
        refreshing={this.loading}
        onRefresh={() => this.fetchItem()}
        numColumns={2}
        renderItem={({item}) => (
          <View style={{width: '50%'}}>
            <ProductItemComponent inline={false} item={item}/>
          </View>
        )}
        data={this.item.products}
      />
    );
  }
}

export default CategoryScreen;
