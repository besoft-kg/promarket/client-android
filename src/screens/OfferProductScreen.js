import React from 'react';
import {Button, Card, Colors, Dialog, Divider, List, Portal, Text, Title} from 'react-native-paper';
import {
  View,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  Image,
  FlatList,
  Linking,
  RefreshControl,
  Share,
} from 'react-native';
import {inject, observer, Observer} from 'mobx-react';
import BaseScreen from './BaseScreen';
import {action, observable} from 'mobx';
import {OFFICE_API_URL, requester} from '../utils/settings';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {numberWithSpaces, sizeNormalizer, windowSize} from '../utils';
import Product from '../models/Product';
import BranchItemComponent from '../components/BranchItemComponent';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {HeaderButton, HeaderButtons, Item} from 'react-navigation-header-buttons';
import MaterialIcons from 'react-native-vector-icons/MaterialCommunityIcons';

@inject('store') @observer
class OfferProductScreen extends BaseScreen {

  @observable item = null;
  @observable loading = false;
  @observable active_image = 0;
  @observable selected_color = null;
  @observable article_dialog = false;
  @observable _store = null;
  @observable branches = [];
  @observable used = null;


  carousel_ref = React.createRef();

  @action fetchItem = () => {
    if (this.loading) {
      return;
    }
    this.setValue('loading', true);
    requester.get('/offer/product', {id: this.props.route.params.offer.id}, true)
      .then((response) => {
        this.setValue('item', response.data.product);
        this.setValue('branches', response.data.branches);
        this.setValue('_store', response.data.store);
        this.setValue('used', response.data.used);
        console.log(response.data);
      }).catch((e) => {
      console.log(e);
    }).finally(() => {
      this.setValue('loading', false);
    });
  };

  share = async () => {
    try {
      const result = await Share.share({
        message: `https://promarket.kg/offer/${this.offer.id}`,
        title: `${this.item.title} в ${this._store.title}`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
        } else {
        }
      } else if (result.action === Share.dismissedAction) {
      }
    } catch (error) {
      alert(error.message);
    }
  };

  componentDidMount() {
    this.fetchItem();
  }

  @action setSelectedColor = (v) => {
    this.selected_color = this.selected_color !== v.id ? v.id : null;
    this.active_image = 0;
    this.carousel_ref.snapToItem(0);
  };

  constructor(props) {
    super(props);

    this.offer = this.props.route.params.offer;
    this.item = new Product(this.offer.product);
    this._store = this.props.route.params.store;

    this.props.navigation.setOptions({
      title: this.item.title,
      headerRight: () => (
        <HeaderButtons
          HeaderButtonComponent={props => (
            <HeaderButton IconComponent={MaterialIcons} iconSize={23} color="#fff" {...props} />
          )}>
          <Item title={'plus'} onPress={() => this.share()} iconName={'share'}/>
        </HeaderButtons>
      ),
    });
  }

  render() {
    const filtered_images = this.item.images.filter(v => (v.color_id === this.selected_color) || !this.selected_color);
    return (
      <>
        <StatusBar translucent backgroundColor="transparent"/>
        <ScrollView refreshControl={<RefreshControl refreshing={this.loading} onRefresh={() => this.fetchItem()}/>}>
          <View style={{alignItems: 'center', paddingVertical: 16, backgroundColor: '#fff'}}>
            <Carousel
              ref={ref => this.carousel_ref = ref}
              containerCustomStyle={{marginBottom: 10}}
              layout={'default'}
              data={filtered_images}
              sliderWidth={windowSize.width}
              itemWidth={windowSize.width}
              onSnapToItem={index => this.setValue('active_image', index)}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductImageModal',
                  {item: filtered_images, index: this.active_image})} style={{alignItems: 'center'}}
                >
                  <Image style={{backgroundColor: '#ffffff', width: '100%', height: 150}} resizeMode={'contain'}
                         source={{uri: `${OFFICE_API_URL}/${item.image.path.original}`}}/>
                </TouchableOpacity>
              )}
            />
            <Pagination
              dotsLength={filtered_images.length}
              activeDotIndex={this.active_image}
              containerStyle={{marginVertical: -20}}
              dotStyle={{
                width: 6,
                height: 6,
                marginHorizontal: -5,
                backgroundColor: 'rgba(81,187,17,0.99)',
              }}
            />
          </View>
          <View style={{margin: 8}}>
            <View>
              <Title>{this.item.title}</Title>
            </View>

            <View style={{
              flex: 1,
              flexDirection: 'row',
              marginHorizontal: 8,
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
              <View flexDirection={'row'}>
                {this.item.colors.map(v => (
                  <TouchableOpacity onPress={() => this.setSelectedColor(v)} style={{
                    marginRight: 3,
                    borderRadius: 30,
                    borderWidth: this.selected_color === v.id ? 3 : 1,
                    borderColor: '#fff',
                    width: 30,
                    height: 30,
                    backgroundColor: `#${v.hex_code}`,
                  }}/>
                ))}
              </View>

              {this.item.brand && (
                <View>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('BrandScreen', {id: this.item.brand.id})}>
                    {this.item.brand.logo ? (
                      <Image style={{width: 40, height: 40}} resizeMode={'contain'}
                             source={{uri: `${OFFICE_API_URL}/${this.item.brand.logo.path.original}`}}/>
                    ) : (
                      <Text style={{textAlign: 'right'}}>{this.item.brand.title}</Text>
                    )}
                  </TouchableOpacity>
                </View>
              )}
            </View>
          </View>

          <Card style={{marginBottom: 8, marginHorizontal: 8, padding: 8}}>
            {/*{title ? (*/}
            {/*  <View justifyContent={'space-between'} flexDirection={'row'}>*/}
            {/*    <Text style={{color: '#808080'}}>{title}</Text>*/}
            {/*    <Text>{item.store.title}</Text>*/}
            {/*  </View>*/}
            {/*) : null}*/}
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              {this.offer.discount_active === 1 ? (
                <Title style={{color: 'red'}}>{this.offer.sale_price}{' '}
                  <Title style={{textDecorationLine: 'underline', color: 'red'}}>с</Title>
                  <Text>{' '}
                    <Text style={{
                      fontSize: 13,
                      textDecorationLine: 'line-through',
                      textDecorationStyle: 'solid',
                      color: '#808080',
                    }}>{this.offer.price}{' '}
                      <Text style={{textDecorationLine: 'underline', color: '#808080'}}>с</Text>
                    </Text>
                  </Text>
                </Title>
              ) : (
                <Title>{numberWithSpaces(this.offer.sale_price)}{' '}<Title style={{textDecorationLine: 'underline'}}>с</Title></Title>
              )}
              {this.used > 0 && (<Text style={{
                  color: 'white',
                  paddingHorizontal: 8,
                  width: 40,
                  marginLeft: 30,
                  backgroundColor: 'blue',
                  borderRadius: 5,
                }}>Б/У</Text>
              )}
              {this.offer.discount_active === 1 && (
                <Text style={{
                  padding: 5,
                  backgroundColor: Colors.red700,
                  color: '#fff',
                  borderRadius: 10,
                }}>-{this.offer.discount_percentage}%</Text>
              )}
            </View>
            {/*{!title && (*/}
            {/*  <Text>{item.store.title}</Text>*/}
            {/*)}*/}
          </Card>

          <Title style={{marginHorizontal: 8, color: '#808080'}}>Где купить?</Title>

          <FlatList
            style={{marginHorizontal: 8}}
            renderItem={({item}) => (
              <Observer>
                {() => (
                  <BranchItemComponent item={{
                    branch: item,
                    store: this._store,
                  }}/>
                )}
              </Observer>
            )}
            data={this.branches}
          />

          {this.item.brief_info.length > 0 && (
            <Card style={{marginHorizontal: 8, marginBottom: 8}}>
              <View style={{marginLeft: 8, paddingBottom: sizeNormalizer(5)}}>
                <Title style={{fontSize: 14}}>Коротко о товаре</Title>
                {this.item.brief_info.map(v => (
                  <>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Icon style={{color: '#32CD32', paddingRight: 5}} size={8} name={'checkbox-blank-circle'}/>
                      <Text>{v.value}</Text>
                    </View>
                  </>
                ))}
                {/*<Divider style={{marginTop: 5}}/>*/}
                {/*<TouchableOpacity*/}
                {/*  onPress={() => this.props.navigation.navigate('ProductSpecificationsScreen', {item: this.item})}*/}
                {/*  style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}*/}
                {/*>*/}
                {/*  <Title style={{fontSize: 12}}>Все характеристики</Title>*/}
                {/*  <Icon style={{paddingRight: 5}} size={20} name={'chevron-right'}/>*/}
                {/*</TouchableOpacity>*/}
              </View>
            </Card>
          )}

          {this.item.articles.length > 0 && (
            <Card style={{padding: sizeNormalizer(8), marginHorizontal: sizeNormalizer(8), marginBottom: sizeNormalizer(8)}}
                  onPress={() => this.setValue('article_dialog', true)}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <Text style={{flex: 1, justifyContent: 'flex-end', marginTop: 5, flexDirection: 'column'}}>Обзоры
                  ({this.item.articles.length})</Text>
                <Icon color={'red'} size={30} name="youtube"/>
              </View>
            </Card>
          )}

          <Portal>
            <Dialog visible={this.article_dialog} onDismiss={() => this.setValue('article_dialog', false)}>
              {this.item.articles.map(v => (
                <List.Item
                  titleStyle={{color: '#33A5DB', textDecorationLine: 'underline', marginVertical: -10}}
                  onPress={() => {
                    Linking.openURL(v.url);
                  }} title={v.title}
                  left={props =>
                    <View justifyContent={'center'}
                          style={{...props, marginLeft: -15, marginRight: -15, marginVertical: -10}}>
                      <List.Icon color={'red'} icon="youtube"/>
                    </View>
                  }/>
              ))}
              <Dialog.Actions>
                <Button onPress={() => this.setValue('article_dialog', false)}>Закрыть</Button>
              </Dialog.Actions>
            </Dialog>
          </Portal>

        </ScrollView>
      </>
    );
  }
}

export default OfferProductScreen;
