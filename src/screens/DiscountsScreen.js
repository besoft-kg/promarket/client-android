import React from 'react';
import {Title} from 'react-native-paper';
import {FlatList, Image, StatusBar, View} from 'react-native';
import BaseComponent from '../components/BaseComponent';
import {inject, observer} from 'mobx-react';
import {action, observable, values} from 'mobx';
import {OFFICE_API_URL, requester} from '../utils/settings';
import ProductItemComponent from '../components/ProductItemComponent';
import {SvgUri} from 'react-native-svg';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import IProduct from '../models/IProduct';
// import CategoriesComponent from '../components/CategoriesComponent';

@inject('store')
@observer
class DiscountsScreen extends BaseComponent {
  @observable item = null;
  @observable items = [];
  @observable loading = false;

  constructor(props) {
    super(props);

    this.props.navigation.setOptions({
      headerShown: false,
    });

    this.item = typeof this.props.route.params === 'object' && 'item' in this.props.route.params ? this.props.route.params.item : null;
  }

  @action fetchItem = () => {
    if (this.loading) {
      return;
    }

    this.setValue('loading', true);
    requester.get('/discount', {
      category_id: this.item ? this.item.id : null,
    }, true).then((response) => {
      this.setValue('items', response.data.map(v => new IProduct(v)));
    }).catch((e) => {
      console.log(e);
    }).finally(() => {
      this.setValue('loading', false);
    });
  };

  componentDidUpdate(prevProps) {
    if (prevProps.route.params !== this.props.route.params) {
      this.fetchItem();
    }
  }

  componentDidMount() {
    this.fetchItem();
    this.unsubsriber = this.props.navigation.addListener('focus', () => {
      StatusBar.setBarStyle('dark-content');
    });
  }

  componentWillUnmount() {
    this.unsubsriber && this.unsubsriber();
  }

  render() {
    return (
      <View style={{padding: 6, marginTop: StatusBar.currentHeight}}>
        <FlatList
          ListHeaderComponent={(
            <>
              {this.item ? (
                <>
                  {this.item.icon && (
                    <View alignItems={'center'}>
                      {this.item.icon.extension === 'svg' ? (
                        <SvgUri
                          width={50}
                          height={50}
                          fill="black"
                          uri={`${OFFICE_API_URL}/${this.item.icon.path.original}`}
                        />
                      ) : (
                        <Image
                          resizeMode={'contain'}
                          style={{width: 200, height: 100}}
                          source={{uri: `${OFFICE_API_URL}/${this.item.icon.path.original}`}}
                        />
                      )}
                    </View>
                  )}
                  <View alignItems={'center'}>
                    <Title>{this.item.title}</Title>
                  </View>
                </>
              ) : (
                <View alignItems={'center'}>
                  <Title>Скидки в ProMarket</Title>
                </View>
              )}
              {/*{this.items.length > 0 && (*/}
              {/*  <CategoriesComponent*/}
              {/*    onSelectItem={item => this.props.navigation.push(`DiscountsScreen`, {item})}*/}
              {/*    items={values(this.categoryStore.items).filter(v => v.parent_id === (this.item ? this.item.id : null) && v.products.length > 0)}*/}
              {/*  />*/}
              {/*)}*/}
            </>
          )}
          ListEmptyComponent={(
            <>
              {!this.loading && (
                <View justifyContent={'center'}
                      style={{flex: 1, flexDirection: 'column', alignItems: 'center', marginTop: 50, color: '#808080'}}>
                  <Icon size={50} name={'sale'} color={'#808080'}/>
                  <Title style={{color: '#808080'}}>Нет скидок</Title>
                </View>
              )}
            </>
          )}
          refreshing={this.loading}
          onRefresh={() => this.fetchItem()}
          numColumns={2}
          renderItem={({item}) => (
            <View style={{width: '50%'}}>
              <ProductItemComponent discount inline={false} item={item}/>
            </View>
          )}
          data={this.items}
        />
      </View>
    );
  }
}

export default DiscountsScreen;
