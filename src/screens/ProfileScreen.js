import React from 'react';
import {
  Divider,
  List,
  Text,
  Portal,
  Dialog,
  Button,
  Title,
  TextInput,
  Menu, Caption,
} from 'react-native-paper';
import {View, ScrollView, StatusBar, Linking, Alert} from 'react-native';
import BaseComponent from '../components/BaseComponent';
import {inject, observer} from 'mobx-react';
import {observable} from 'mobx';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {APP_VERSION, requester} from '../utils/settings';
import PhoneNumber from '../utils/PhoneNumber';
import {sizeNormalizer} from '../utils';

@inject('store') @observer
class ProfileScreen extends BaseComponent {
  @observable problem_report = false;
  @observable dialog_support = false;
  @observable text = '';
  @observable phone_number = '';

  constructor(props) {
    super(props);

    this.phone_number_object = new PhoneNumber();
  }

  clean = () => {
    this.setValue('text', '');
    this.setValue('phone_number', '');
    this.setValue('problem_report', false);
  };

  whatsapp = () => {
    Linking.openURL('whatsapp://send?text=&phone=+996777171171');
  };

  telegram = () => {
    Linking.openURL('https://t.me/tabekg');
  };

  call = () => {
    Linking.openURL(`tel:+${996777171171}`);
  };

  instagram = () => {
    Linking.openURL('https://www.instagram.com/promarket.kg');
  };

  fetchPost = () => {
    if (!this.phone_number_object.isValid() || this.text.length <= 0) {
      return;
    }
    requester.post('/support', {
      text: this.text,
      phone_number: this.phone_number_object.getE164Format(),
    }, true).then((response) => {
      Alert.alert('Успешно', 'Успешно отправлена!');
      this.clean();
    }).catch((e) => {
      console.log(e);
    });
  };

  partner = () => {
    Alert.alert(
      'Сотрудничество с нами',
      'Чтобы сотрудничать с нами обращайтесь по номеру +996 777 171 171 или через Instagram @promarket.kg',
      [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
        {
          text: 'Instagram',
          onPress: () => this.instagram(),
        },
        {
          text: 'WhatsApp',
          onPress: () => this.whatsapp(),
        },
      ],
      {cancelable: true},
    );
  };

  setPhoneNumber = n => {
    this.phone_number_object.parseFromString(n);
    this.setValue('phone_number', n);
  };

  render() {
    return (
      <>
        <StatusBar translucent backgroundColor="transparent"/>
        <ScrollView>
          {/*<List.Item*/}
          {/*  title={this.appStore.region ? this.appStore.region.title : 'Кыргызстан'}*/}
          {/*  onPress={() => this.appStore.setValue('region_show', true)}*/}
          {/*  left={props => <List.Icon {...props} icon="map-marker-outline"/>}*/}
          {/*/>*/}
          {/*<Divider/>*/}
          <List.Item
            title={'У вас есть магазин?'}
            description={'Хотите чтобы, ваш магазин появился в нашем приложении?'}
            onPress={() => this.partner()}
            left={props => <List.Icon {...props} icon="handshake"/>}
          />
          <Divider/>
          <List.Item
            title="О нас"
            onPress={() => this.props.navigation.navigate('AboutUsScreen')}
            left={props => <List.Icon {...props} icon="briefcase-variant"/>}
            right={props => <List.Icon {...props} icon="chevron-right"/>}
          />
          {/*<Divider/>*/}
          {/*<List.Item*/}
          {/*  onPress={() => this.props.navigation.navigate('HelpScreen')}*/}
          {/*  title="Помощь"*/}
          {/*  left={props => <List.Icon {...props} icon="help"/>}*/}
          {/*  right={props => <List.Icon {...props} icon="chevron-right"/>}*/}
          {/*/>*/}
          <Divider/>
          <List.Item
            onPress={() => this.setValue('problem_report', true)}
            title="Сообщить о проблеме"
            left={props => <List.Icon {...props} icon="file-document-edit-outline"/>}
          />
          <Divider/>
          {/*<List.Item*/}
          {/*  title="Пользовательское соглашение"*/}
          {/*  left={props => <List.Icon {...props} icon="handshake"/>}*/}
          {/*/>*/}
          {/*<Divider/>*/}
          <List.Item
            title="Политика конфиденциальности"
            right={props => <List.Icon {...props} icon="chevron-right"/>}
            onPress={() => Linking.openURL('https://promarket.kg/privacy')}
            left={props => <List.Icon {...props} icon="file-document-outline"/>}
          />
          <Divider/>
          <List.Item
            onPress={() => this.setValue('dialog_support', true)}
            title="Служба поддержки"
            left={props => <List.Icon {...props} icon="lifebuoy"/>}
          />
          <Divider/>

          <Caption style={{fontSize: sizeNormalizer(17), textAlign: 'center', marginTop: sizeNormalizer(18)}}>Кыргызстанда
            жасалган</Caption>
          <Caption style={{fontSize: sizeNormalizer(17), textAlign: 'center', marginTop: sizeNormalizer(3)}}>Сделано в
            Кыргызстане</Caption>
          <Caption style={{fontSize: sizeNormalizer(17), textAlign: 'center', marginTop: sizeNormalizer(3)}}>Made in
            Kyrgyzstan</Caption>

          <Caption style={{textAlign: 'center', marginTop: sizeNormalizer(18)}}>Версия
            приложения: {APP_VERSION}</Caption>
          <Caption style={{textAlign: 'center', marginBottom: sizeNormalizer(18)}}>ProMarket by Besoft</Caption>
        </ScrollView>

        <Portal>
          <Dialog visible={this.dialog_support} onDismiss={() => this.setValue('dialog_support', false)}>
            <Dialog.Content>
              <Menu.Item icon="whatsapp" onPress={() => this.whatsapp()} title="+996777171171"/>
              <Menu.Item icon="phone" onPress={() => this.call()} title="+996777171171"/>
              <Menu.Item icon="telegram" onPress={() => this.telegram()} title="Support ProMarket"/>
              <Menu.Item icon="instagram" onPress={() => this.instagram()} title="promarket.kg"/>
            </Dialog.Content>
            <Dialog.Actions>
              <Button onPress={() => this.setValue('dialog_support', false)}>Закрыть</Button>
            </Dialog.Actions>
          </Dialog>

          <Dialog visible={this.problem_report} onDismiss={() => this.setValue('problem_report', false)}>
            <View style={{alignItems: 'flex-start', padding: 8}}>
              <Title>Чем мы можем помочь?</Title>

              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={{fontSize: 16}}>Столкнулись с неисправностью или хотите узнать, как использовать
                  возможности ProMarket по максимуму? Наша команда поможет вам:</Text>
              </View>

              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon style={{color: '#483D8B', paddingRight: 5, transform: [{rotate: '90deg'}]}} size={8}
                      name={'triangle'}/>
                <Text>Найти оптимальные решения.</Text>
              </View>

              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon style={{color: '#483D8B', paddingRight: 5, transform: [{rotate: '90deg'}]}} size={8}
                      name={'triangle'}/>
                <Text>Разобраться в технических и сервисных нюансах платформы.</Text>
              </View>

              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon style={{color: '#483D8B', paddingRight: 5, transform: [{rotate: '90deg'}]}} size={8}
                      name={'triangle'}/>
                <Text>Изучить наши правила в области авторских прав.</Text>
              </View>
            </View>
            <TextInput
              label="Сообщить о проблеме"
              multiline
              defaultValue={this.text}
              onChangeText={(n) => this.setValue('text', n)}
            />
            <TextInput
              label="Номер телефона"
              keyboardType="phone-pad"
              value={this.phone_number}
              onChangeText={(n) => this.setPhoneNumber(n)}
            />
            <Dialog.Actions>
              <Button disabled={!this.phone_number_object.isValid() || this.text.length <= 0}
                      onPress={() => this.fetchPost()}>Отправить</Button>
              <Button onPress={() => this.clean()}>Закрыть</Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
      </>
    );
  }

}

export default ProfileScreen;
