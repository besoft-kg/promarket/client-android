import React from 'react';
import {Searchbar, Text, Divider, Title, Card, List} from 'react-native-paper';
import BaseComponent from '../components/BaseComponent';
import {ScrollView, StatusBar, View, TouchableOpacity, RefreshControl, FlatList, Image} from 'react-native';
import {action, observable} from 'mobx';
import {requester} from '../utils/settings';
import {inject, observer} from 'mobx-react';
import storage from '../utils/storage';
import ProductItemComponent from '../components/ProductItemComponent';

@inject('store')
@observer
class SearchScreen extends BaseComponent {
  @observable query = '';
  @observable prompts = [];
  @observable item = {};
  @observable history = [];
  @observable no_results = false;

  clearHistory = () => {
    this.setValue('history', []);
    storage.remove('search_history').then();
  };

  @action fetchPrompts = (text) => {
    this.query = text;
    this.setValue('item', {});
    requester.get('/search/prompt', {
      query: this.query,
    }, true).then((response) => {
      this.setValue('prompts', response.data);
    }).catch((e) => {
      console.log(e);
    });
  };

  componentDidMount() {
    this.fetchHistory().then(() => {
    });
  }

  fetchHistory = async () => {
    this.setValue('history', await storage.get('search_history', []));
  };

  @action fetchResults = (v) => {
    this.setValue('query', v);
    this.setValue('prompts', []);
    this.setValue('no_results', true);

    if (!this.history.find(g => g === v)) {
      this.history = [v, ...this.history];
      storage.set('search_history', this.history).then();
    }

    this.setValue('loading', true);
    requester.get('/search/result', {
      query: this.query,
    }, true).then((response) => {

      this.setValue('loading', false);
      this.setValue('item', response.data);

      this.productStore.createOrUpdate(response.data.products);
      this.storeStore.createOrUpdate(response.data.stores);
      this.brandStore.createOrUpdate(response.data.brands);

    }).catch((e) => {
      console.dir(e);
    });
  };

  render() {
    return (
      <View style={{marginTop: StatusBar.currentHeight}} flex={1}>
        <Searchbar
          autoFocus
          onSubmitEditing={() => this.fetchResults(this.query)}
          inputStyle={{borderBottomWidth: 0}}
          style={{margin: 8}}
          value={this.query}
          onChangeText={(text) => this.fetchPrompts(text)}
          placeholder="Что вы ищете..."
        />
        <ScrollView keyboardShouldPersistTaps={'always'} style={{flexGrow: 1}}
                    refreshControl={<RefreshControl refreshing={this.loading}
                                                    onRefresh={() => this.fetchResults(this.query)}/>}>
          {this.query.length > 0 && this.prompts.map(v => (
            <TouchableOpacity onPress={() => this.fetchResults(v.query)}>
              <View style={{padding: 8}}>
                <Text style={{fontSize: 18}}>{v.query}</Text>
                <Divider/>
              </View>
            </TouchableOpacity>
          ))}

          {this.history.length > 0 && this.query.length === 0 && (
            <>
              <Title style={{marginVertical: 2, marginLeft: 8, color: '#808080'}}>
                История поиска
              </Title>
              {this.history.map(v => (
                <TouchableOpacity onPress={() => this.fetchResults(v)}>
                  <View style={{padding: 8}}>
                    <Text style={{fontSize: 18}}>{v}</Text>
                    <Divider/>
                  </View>
                </TouchableOpacity>
              ))}
              <TouchableOpacity onPress={() => this.clearHistory()}>
                <View style={{padding: 8}}>
                  <Text style={{fontSize: 18, color: '#0066CC'}}>Очистить историю</Text>
                </View>
              </TouchableOpacity>
            </>
          )}

          {this.no_results && 'products' in this.item && this.item.products.length <= 0 &&
          'categories' in this.item && this.item.categories.length <= 0 &&
          'brands' in this.item && this.item.brands.length <= 0 &&
          'stores' in this.item && this.item.stores.length <= 0 && (
            <>
              <View style={{marginHorizontal: 8}}>
                <View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                  <Image
                    style={{width: 80, height: 80, opacity: 0.7}}
                    resizeMode={'cover'}
                    source={{
                      uri: 'https://static.thenounproject.com/png/744760-200.png',
                    }}
                  />
                  <Title style={{marginBottom: 50, textAlign: 'center', color: '#474343'}}>
                    К сожалению, по вашему запросу ничего не найдено.
                  </Title>
                </View>
                <Title>Обратите внимание на популярные товары</Title>
                <FlatList
                  numColumns={2}
                  renderItem={({item}) => (
                    <View style={{width: '50%'}}>
                      <ProductItemComponent inline={false} item={item}/>
                    </View>
                  )}
                  data={this.productStore.popular.slice()}
                />
              </View>
            </>
          )}

          {'products' in this.item && this.item.products.length > 0 && (
            <Card style={{marginBottom: 8, marginHorizontal: 8}}>
              <Title style={{marginHorizontal: 8}}>Найденные товары</Title>
              {this.item.products.map(p => (
                <>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('ProductScreen', {id: p.id})}>
                    <List.Item
                      title={p.title}
                      left={props => <List.Icon {...props} icon="magnify"/>}
                    />
                  </TouchableOpacity>
                  <Divider/>
                </>
              ))}
            </Card>
          )}

          {'categories' in this.item && this.item.categories.length > 0 && (
            <Card style={{marginBottom: 8, marginHorizontal: 8}}>
              <Title style={{marginHorizontal: 8}}>Найденные категории</Title>
              {this.item.categories.map(c => (
                <>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('CategoryScreen', {item: c})}>
                    <List.Item
                      title={c.title}
                      left={props => <List.Icon {...props} icon="magnify"/>}
                    />
                  </TouchableOpacity>
                  <Divider/>
                </>
              ))}
            </Card>
          )}

          {'brands' in this.item && this.item.brands.length > 0 && (
            <Card style={{marginBottom: 8, marginHorizontal: 8}}>
              <Title style={{marginHorizontal: 8}}>Найденные бренды</Title>
              {this.item.brands.map(b => (
                <>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('BrandScreen', {id: b.id})}>
                    <List.Item
                      title={b.title}
                      left={props => <List.Icon {...props} icon="magnify"/>}
                    />
                  </TouchableOpacity>
                  <Divider/>
                </>
              ))}
            </Card>
          )}

          {'stores' in this.item && this.item.stores.length > 0 && (
            <Card style={{marginBottom: 8, marginHorizontal: 8}}>
              <Title style={{marginHorizontal: 8}}>Найденные магазины</Title>
              {this.item.stores.map(s => (
                <>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('StoreScreen', {id: s.id})}>
                    <List.Item
                      title={s.title}
                      left={props => <List.Icon {...props} icon="magnify"/>}
                    />
                  </TouchableOpacity>
                  <Divider/>
                </>
              ))}
            </Card>
          )}
        </ScrollView>
      </View>
    );
  }
}

export default SearchScreen;
