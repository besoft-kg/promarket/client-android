import React from 'react';
import BaseComponent from '../components/BaseComponent';
import {StatusBar, Text, View, ScrollView, RefreshControl, Share, TouchableOpacity, Linking, Alert} from 'react-native';
import {inject, observer} from 'mobx-react';
import {action, observable} from 'mobx';
import {OFFICE_API_URL, requester} from '../utils/settings';
import {Image} from 'react-native';
import {Title, List, Card, Dialog, Button, TextInput, Portal, Colors} from 'react-native-paper';
import BranchItemComponent from '../components/BranchItemComponent';
import Offer from '../models/Offer';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import PhoneNumber from '../utils/PhoneNumber';
import {numberWithSpaces} from '../utils';

@inject('store')
@observer
class OfferScreen extends BaseComponent {
  @observable item = null;
  @observable loading = false;
  @observable phone_dialog = false;
  @observable about_dialog = false;
  @observable complain_dialog = false;
  @observable text = '';
  @observable phone_number = '';

  constructor(props) {
    super(props);

    this.item = new Offer('item' in this.props.route.params ? this.props.route.params.item : {id: this.props.route.params.id});
    this.product = this.item.product;
    this.phone_number_object = new PhoneNumber();
  }

  componentDidMount() {
    this.unsubsriber = this.props.navigation.addListener('focus', () => {
      StatusBar.setBarStyle('light-content');
    });

    this.fetchItem();
  }

  setPhoneNumber = n => {
    this.phone_number_object.parseFromString(n);
    this.setValue('phone_number', n);
  };

  componentWillUnmount() {
    this.unsubsriber && this.unsubsriber();
  }

  clean = () => {
    this.setValue('complain_dialog', false);
    this.setValue('text', '');
    this.setValue('phone_number', '');
  };

  renderContact = (v) => {

    let value = v.value;
    let icon = 'phone';
    let link = '';

    switch (v.type) {
      case 'whatsapp':
        value = `+${v.value}`;
        icon = 'whatsapp';
        link = `http://api.whatsapp.com/send?phone=${v.value}`;
        break;
      case 'phone':
        value = `+${v.value}`;
        icon = 'phone';
        link = `tel:+${v.value}`;
        break;
      case 'telegram':
        value = `${v.value}`;
        icon = 'telegram';
        link = `https://t.me/${v.value}`;
        break;
      case 'instagram':
        value = `${v.value}`;
        icon = 'instagram';
        link = `https://www.instagram.com/${v.value}`;
        break;
    }

    return (
      <TouchableOpacity
        onPress={() => Linking.openURL(link)}
        style={{flexDirection: 'row', alignItems: 'center', marginBottom: 8}}
      >
        <Icon size={20} name={icon}/>
        <Title style={{marginLeft: 8}}>{value}</Title>
      </TouchableOpacity>
    );
  };

  @action fetchItem = () => {
    if (this.loading) {
      return;
    }
    this.setValue('loading', true);
    requester.get('/offer', {id: this.item.id}, true).then((response) => {
      this.setValue('item', response.data);
      console.log(response.data);
      this.storeStore.createOrUpdate(response.data.store);
    }).catch((e) => {
      console.dir(e);
    }).finally(() => {
      this.setValue('loading', false);
    });

  };

  share = async () => {
    try {
      const result = await Share.share({
        message: `https://promarket.kg/offer/${this.item.id}`,
        title: `${this.item.product.title} в ${this.item.store.title}`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
        } else {
        }
      } else if (result.action === Share.dismissedAction) {
      }
    } catch (error) {
      alert(error.message);
    }
  };

  fetchPost = () => {
    if (!this.phone_number_object.isValid() || this.text.length <= 0) {
      return;
    }
    requester.post('/support', {
      text: this.text,
      phone_number: this.phone_number_object.getE164Format(),
    }, true).then((response) => {
      Alert.alert('Успешно', 'Успешно отправлена!');
      this.clean();
    }).catch((e) => {
      console.log(e);
    });
  };

  render() {
    return (
      <>
        <StatusBar translucent barStyle="dark-content" backgroundColor={'white'}/>
        <ScrollView
          style={{paddingTop: StatusBar.currentHeight}}
          refreshControl={<RefreshControl refreshing={this.loading} onRefresh={() => this.fetchItem()}/>}
        >
          <Card style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            {this.item.store.logo && (
              <Image style={{width: 100, height: 100, alignSelf: 'center'}} resizeMode={'contain'}
                     source={{uri: `${OFFICE_API_URL}/${this.item.store.logo.path.original}`}}/>
            )}
            <Text style={{
              fontSize: 24,
              marginTop: this.item.store.logo ? 0 : 8,
              textAlign: 'center',
              marginBottom: 8,
            }}>{this.item.store.title}</Text>
          </Card>
          <View style={{marginHorizontal: 8}}>
            <View style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-around',
              marginTop: 12,
              marginBottom: 8,
            }}>
              <TouchableOpacity onPress={() => this.setValue('phone_dialog', true)}>
                <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
                  <Icon size={35} name={'phone'}/>
                  <Text style={{color: 'rgba(0, 0, 0, .7)'}}>Связаться</Text>
                </View>
              </TouchableOpacity>
              <View style={{height: '70%', width: 1, backgroundColor: '#DCDCDC'}}/>
              <TouchableOpacity onPress={() => this.setValue('about_dialog', true)}>
                <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
                  <Icon size={35} name={'newspaper-variant'}/>
                  <Text style={{color: 'rgba(0, 0, 0, .7)'}}>О магазине</Text>
                </View>
              </TouchableOpacity>
              <View style={{height: '70%', width: 1, backgroundColor: '#DCDCDC'}}/>
              <TouchableOpacity onPress={() => this.setValue('complain_dialog', true)}>
                <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
                  <Icon size={35} name={'alert-octagon'}/>
                  <Text style={{color: 'rgba(0, 0, 0, .7)'}}>Пожаловаться</Text>
                </View>
              </TouchableOpacity>
              <View style={{height: '70%', width: 1, backgroundColor: '#DCDCDC'}}/>
              <TouchableOpacity onPress={() => this.share()}>
                <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
                  <Icon size={35} name={'share-variant'}/>
                  <Text style={{color: 'rgba(0, 0, 0, .7)'}}>Поделиться</Text>
                </View>
              </TouchableOpacity>
            </View>
            <Button icon="store" mode="contained" color={Colors.grey800}
                    onPress={() => this.props.navigation.navigate('StoreScreen', {id: this.item.store.id})}>
              В магазин
            </Button>

            <View>
              <List.Item
                right={props => (
                  this.item.discount_active === 1 && (
                    <View {...props} alignItems={'center'}>
                      <Text style={{color: 'red'}}>Скидка {this.item.discount_percentage}%</Text>
                      <Text style={{color: '#0E82B8'}}>от {this.item.discount_start_on}</Text>
                      <Text style={{color: '#78AFC9'}}>до {this.item.discount_end_on}</Text>
                    </View>
                  )
                )}
                title={(
                  <View flex={1} style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Text>
                      {' '}
                    </Text>
                    <Text style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
                      <Title>
                        <Title>{this.item.discount_active === 1 ? (
                          <Text
                            style={{color: 'red'}}>
                            {numberWithSpaces(this.item.sale_price)}{' '}
                            <Title style={{textDecorationLine: 'underline', color: 'red'}}>с</Title>
                          </Text>
                        ) : (
                          <Text>{numberWithSpaces(this.item.price)} <Title style={{textDecorationLine: 'underline'}}>с</Title></Text>)}
                        </Title>
                        {this.item.discount_active === 1 && this.item.store && (
                          <Text>
                            {' '}
                            <Text style={{
                              fontSize: 15,
                              textDecorationLine: 'line-through',
                              textDecorationStyle: 'solid',
                              color: '#808080',
                            }}>
                               {numberWithSpaces(this.item.price)}{' '}
                              <Text style={{textDecorationLine: 'underline'}}>с</Text></Text>
                          </Text>
                        )}
                      </Title>
                    </Text>
                  </View>
                )}
                left={props =>
                  <View {...props}>
                    {this.product.main_image && (
                      <Image source={{uri: `${OFFICE_API_URL}/${this.product.main_image.path.original}`}}
                             style={{width: 25, height: 47, marginRight: 2}}/>
                    )}
                  </View>
                }
              />
            </View>
            {'branches' in this.item && this.item.branches.length > 0 && (
              <View>
                <Title style={{marginLeft: 8, marginVertical: 8, color: '#808080'}}>Где купить?</Title>
                {this.item.branches.map(v => (
                  <BranchItemComponent item={{branch: v, store: this.item.store}}/>
                ))}
              </View>
            )}
          </View>

          <Portal>
            <Dialog visible={this.phone_dialog} onDismiss={() => this.setValue('phone_dialog', false)}>
              <Dialog.Content>
                {(this.item.store.contacts || []).length > 0 ? (
                  <>
                    {(this.item.store.contacts || []).map((v, i) => this.renderContact(v))}
                  </>
                ) : (
                  <Title>Нет контактов</Title>
                )}
              </Dialog.Content>
              <Dialog.Actions>
                <Button onPress={() => this.setValue('phone_dialog', false)}>Закрыть</Button>
              </Dialog.Actions>
            </Dialog>

            <Dialog style={{marginVertical: 130}} visible={this.about_dialog}
                    onDismiss={() => this.setValue('about_dialog', false)}>
              <Dialog.Title>Магазин {this.item.store.title}</Dialog.Title>
              <Dialog.ScrollArea>
                <ScrollView style={{marginVertical: 8}}>
                  <Text>{this.item.store.about}</Text>
                </ScrollView>
              </Dialog.ScrollArea>
              <Dialog.Actions>
                <Button onPress={() => this.setValue('about_dialog', false)}>Закрыть</Button>
              </Dialog.Actions>
            </Dialog>

            <Dialog visible={this.complain_dialog} onDismiss={() => this.setValue('complain_dialog', false)}>
              <View style={{margin: 8}}>
                <Title>Уважаемые покупатели!</Title>
                <Text>Если Вы купили некачественный товар в нашем магазине,
                  сообщите об этом. Ваше сообщение получат магазин и Админимстрация ProMarket!
                </Text>
                <TextInput
                  label="Сообщить о проблеме"
                  multiline
                  defaultValue={this.text}
                  onChangeText={(n) => this.setValue('text', n)}
                />
                <TextInput
                  keyboardType="phone-pad"
                  label="Номер телефона"
                  value={this.phone_number}
                  onChangeText={(n) => this.setPhoneNumber(n)}
                /></View>
              <Dialog.Actions>
                <Button disabled={!this.phone_number_object.isValid() || this.text.length <= 0}
                        onPress={() => this.fetchPost()}>Отправить</Button>
                <Button onPress={() => this.clean()}>Закрыть</Button>
              </Dialog.Actions>
            </Dialog>
          </Portal>
        </ScrollView>
      </>
    );
  }
}

export default OfferScreen;
