import React from 'react';
import {Title as RNPTitle} from 'react-native-paper';
import {sizeNormalizer} from '../utils';

class Title extends React.Component {
  static defaultProps = {
    style: {},
  };

  render() {
    const style = {
      ...this.props.style,
      fontSize: sizeNormalizer(18),
    };

    return (
      <RNPTitle {...this.props} style={style}  children={this.props.children}/>
    );
  }
}

export default Title;
