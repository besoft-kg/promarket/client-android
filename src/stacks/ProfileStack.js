import React from 'react'
import BaseComponent from "../components/BaseComponent";
import {observer} from "mobx-react";
import {createStackNavigator} from '@react-navigation/stack';
import AboutUsScreen from "../screens/AboutUsScreen";
import HelpScreen from "../screens/HelpScreen";
import ProfileScreen from "../screens/ProfileScreen";
import {APP_PRIMARY_COLOR} from '../utils/settings';
import {StatusBar} from 'react-native';

const Stack = createStackNavigator();

@observer
class ProfileStack extends BaseComponent {
  componentDidMount() {
    this.unsubsriber = this.props.navigation.addListener('focus', () => {
      StatusBar.setBarStyle('light-content');
    });
  }

  componentWillUnmount() {
    this.unsubsriber && this.unsubsriber();
  }

  render() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: APP_PRIMARY_COLOR,
          },
          headerTintColor: '#fff',
        }}
        initialRouteName={'ProfileScreen'}
      >
        <Stack.Screen
          options={{title: 'Меню'}}
          name={'ProfileScreen'}
          component={ProfileScreen}
        />
        <Stack.Screen
          options={{title: 'О нас'}}
          name={'AboutUsScreen'}
          component={AboutUsScreen}
        />
        <Stack.Screen
          options={{title: 'Помощь'}}
          name={'HelpScreen'}
          component={HelpScreen}
        />
      </Stack.Navigator>
    );
  }
}

export default ProfileStack;
