import React from 'react';
import BaseComponent from '../components/BaseComponent';
import {observer} from 'mobx-react';
import {createStackNavigator} from '@react-navigation/stack';
import ProductScreen from '../screens/ProductScreen';
import {APP_PRIMARY_COLOR} from '../utils/settings';
import BrandScreen from '../screens/BrandScreen';
import ProductSpecificationsScreen from '../screens/ProductSpecificationsScreen';
import OfferScreen from '../screens/OfferScreen';
import FavoritesScreen from '../screens/FavoritesScreen';
import CategoryScreen from '../screens/CategoryScreen';
import StoreScreen from '../screens/StoreScreen';
import OfferProductScreen from '../screens/OfferProductScreen';

const Stack = createStackNavigator();

@observer
class FavoritesStack extends BaseComponent {
  render() {
    return (
      <>
        <Stack.Navigator
          screenOptions={{
            headerStyle: {
              backgroundColor: APP_PRIMARY_COLOR,
            },
            headerTintColor: '#fff',
          }}
          initialRouteName={'FavoritesScreen'}
        >
          <Stack.Screen
            options={{title: 'Избранные'}}
            name={'FavoritesScreen'}
            component={FavoritesScreen}
          />
          <Stack.Screen
            name={'ProductScreen'}
            component={ProductScreen}
          />
          <Stack.Screen
            options={{title: 'Характеристики'}}
            name={'ProductSpecificationsScreen'}
            component={ProductSpecificationsScreen}
          />
          <Stack.Screen
            name={'BrandScreen'}
            component={BrandScreen}
          />
          <Stack.Screen
            name={'OfferScreen'}
            component={OfferScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name={'CategoryScreen'}
            component={CategoryScreen}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name={'StoreScreen'}
            component={StoreScreen}
          />
          <Stack.Screen
            name={'OfferProductScreen'}
            component={OfferProductScreen}
          />
        </Stack.Navigator>
      </>
    );
  }
}

export default FavoritesStack;
