import React from 'react';
import BaseComponent from '../components/BaseComponent';
import {observer} from 'mobx-react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import ProductScreen from '../screens/ProductScreen';
import {APP_PRIMARY_COLOR} from '../utils/settings';
import BrandScreen from '../screens/BrandScreen';
import CategoryScreen from '../screens/CategoryScreen';
import ProductSpecificationsScreen from '../screens/ProductSpecificationsScreen';
import OfferScreen from '../screens/OfferScreen';
import SearchScreen from '../screens/SearchScreen';
import StoreScreen from '../screens/StoreScreen';
import OfferProductScreen from '../screens/OfferProductScreen';
import BrandsScreen from '../screens/BrandsScreen';

const Stack = createStackNavigator();

@observer
class HomeStack extends BaseComponent {
  render() {
    return (
      <>
        <Stack.Navigator
          screenOptions={{
            headerStyle: {
              backgroundColor: APP_PRIMARY_COLOR,
            },
            headerTintColor: '#fff',
          }}
          initialRouteName={'HomeScreen'}
        >
          <Stack.Screen
            options={{headerShown: false}}
            name={'HomeScreen'}
            component={HomeScreen}
          />
          <Stack.Screen
            name={'ProductScreen'}
            component={ProductScreen}
          />
          <Stack.Screen
            options={{title: 'Характеристики'}}
            name={'ProductSpecificationsScreen'}
            component={ProductSpecificationsScreen}
          />
          <Stack.Screen
            name={'BrandScreen'}
            component={BrandScreen}
          />
          <Stack.Screen
            name={'OfferScreen'}
            component={OfferScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name={'CategoryScreen'}
            component={CategoryScreen}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name={'SearchScreen'}
            component={SearchScreen}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name={'StoreScreen'}
            component={StoreScreen}
          />
          <Stack.Screen
            name={'OfferProductScreen'}
            component={OfferProductScreen}
          />
          <Stack.Screen
            name={'BrandsScreen'}
            component={BrandsScreen}
            options={{
              title: 'Все бренды'
            }}
          />
        </Stack.Navigator>
      </>
    );
  }
}

export default HomeStack;
