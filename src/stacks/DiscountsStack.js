import React from 'react'
import BaseComponent from "../components/BaseComponent";
import {observer} from "mobx-react";
import {createStackNavigator} from '@react-navigation/stack';
import DiscountsScreen from "../screens/DiscountsScreen";
import ProductScreen from '../screens/ProductScreen';
import ProductSpecificationsScreen from '../screens/ProductSpecificationsScreen';
import BrandScreen from '../screens/BrandScreen';
import OfferScreen from '../screens/OfferScreen';
import {APP_PRIMARY_COLOR} from '../utils/settings';

const Stack = createStackNavigator();

@observer
class DiscountsStack extends BaseComponent {
  render() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
        initialRouteName={'DiscountsScreen'}
      >
        <Stack.Screen
          name={'DiscountsScreen'}
          component={DiscountsScreen}
        />
        <Stack.Screen
          name={'ProductScreen'}
          component={ProductScreen}
          options={{
            headerStyle: {
              backgroundColor: APP_PRIMARY_COLOR,
            },
            headerTintColor: '#fff',
            headerShown: true,
          }}
        />
        <Stack.Screen
          options={{title: 'Характеристики'}}
          name={'ProductSpecificationsScreen'}
          component={ProductSpecificationsScreen}
        />
        <Stack.Screen
          name={'BrandScreen'}
          component={BrandScreen}
          options={{
            headerStyle: {
              backgroundColor: APP_PRIMARY_COLOR,
            },
            headerTintColor: '#fff',
            headerShown: true,
          }}
        />
        <Stack.Screen
          name={'OfferScreen'}
          component={OfferScreen}
        />
      </Stack.Navigator>
    );
  }
}

export default DiscountsStack;
